﻿using FeedbackHR.Entities;
using FeedbackHR.Infrastructure.Validation;
using FeedbackHR.Repository;
using FeedbackHR.ServiceEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FeedbackHR.Services
{
    public class EndOfProbationService : IEndOfProbationService
    {
        private IRepository<EndOfProbationFormEntity> _repository;
        private EndOfProbationFormEntityValidator _validator;
       
        public EndOfProbationService(IRepository<EndOfProbationFormEntity> repository, EndOfProbationFormEntityValidator validator)
        {
            _repository = repository;
            _validator = validator;
        }
        private bool IsValid(EndOfProbationFormEntity entity)
        {
            var isValidate = _validator.Validate(entity).IsValid;  // Create StartDate get from FMR database API, EndDate StartDate +3 month (recomended) or insert manual
            if (!isValidate)
            {
                return false;
            }
            else return true;
        }
        public async Task<bool> DeleteAsync(string EOPId)
        {
            return await _repository.DeleteByIdAsync(EOPId);
        }

        public async Task<IEnumerable<EndOfProbationFormEntity>> GetAllAsync()
        {
           var result = await _repository.GetAllAsync();
            return result;
        }

        public async Task<EndOfProbationFormEntity> GetByIdAsync(string EOPId)
        {
            var result = await _repository.GetByIdAsync(EOPId);
            return result;
        }

        public void IsFollowUpOpened(EndOfProbationFormEntity entity)
        {
            entity.IsOpenedForFollowUp = false; 
            for(int i = 0; i < entity.FollowUpSection.FollowUpOptions.Count; i++)
            {
                var option = entity.FollowUpSection.FollowUpOptions.ElementAt(i);
                if (option.Checked == true)
                {
                    if (entity.FollowUpsToDo.ElementAt(i).Resolved==false)
                    {
                        entity.IsOpenedForFollowUp = true;
                    }          
                }
            }
        }
        public async Task<bool> UpdateAsync(EndOfProbationFormEntity entity)
        {
            var isValid = IsValid(entity);
            var followUpsToDo = entity.FollowUpsToDo.Where(x => x.Resolved == false).ToList();
            IsFollowUpOpened(entity);
           
            if (isValid)
            {
                
                var result = await _repository.UpdateAsync(entity);
                return result;
            }
            else
            {
              
                throw new ArgumentException("EOP entity is not valid");
            }
        }
    }
}
