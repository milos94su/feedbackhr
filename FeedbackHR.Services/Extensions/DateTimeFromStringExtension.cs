﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace FeedbackHR.Services.Extensions
{
    public static class DateTimeFromStringExtension
    {
        public static DateTime GetDateFromString(this string dateInString)
        {
            //Format of date in string should be like : "dd.MM.yyyy"

            

            var splitOn = '.';

            try
            {
                string[] dateSplit = dateInString.Split(splitOn);

                var day =Convert.ToInt32(dateSplit[0]);
                var month = Convert.ToInt32(dateSplit[1]);
                var year = Convert.ToInt32(dateSplit[2]);

                return new DateTime(year, month, day);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
    }
}
