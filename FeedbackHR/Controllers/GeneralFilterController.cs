﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using FeedbackHR.Entities;
using FeedbackHR.Entities.Filter;
using FeedbackHR.Infrastructure.Cache;
using FeedbackHR.ServiceEntities;
using FeedbackHR.ServiceEntities.SearchService;
using FeedbackHR.ServiceEntities.SearchService.SearchEmployeesService;
using FeedbackHR.ServiceEntities.ServiceModels;
using FeedbackHR.Services;
using FeedbackHR.Services.DataHelpService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Serilog.Sinks.File;

namespace FeedbackHR.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GeneralFilterController : ControllerBase
    {
        private ILogger<GeneralFilterController> _logger;
        private readonly ISearchEmployeesService _searchEmployeesService;
        private readonly ISearchServiceApi<TeamEntity> _searchTeamsService;
        private readonly ISearchServiceApi<UnitEntity> _searchUnitsService;
        private readonly ISearchServiceApi<LocationEntity> _searchLocationsService;
        private readonly ISearchServiceApi<PositionEntity> _searchPositionsService;
        private readonly DataCacheHelper _dataCache;
        private readonly IProfileDataGatherer _profileDataGatherer;

        public GeneralFilterController(ILogger<GeneralFilterController> logger,
            ISearchEmployeesService searchEmployeesService,
            ISearchServiceApi<UnitEntity> searchUnitsService,
            ISearchServiceApi<TeamEntity> searchTeamsService,
            ISearchServiceApi<PositionEntity> searchPositionsService,
            ISearchServiceApi<LocationEntity> searchLocationsService,
            IOptions<CacheSettings> options,
            IProfileDataGatherer profileDataGatherer)
        {
            this._logger = logger;
            this._searchEmployeesService = searchEmployeesService;
            this._searchTeamsService = searchTeamsService;
            this._searchUnitsService = searchUnitsService;
            this._searchLocationsService = searchLocationsService;
            this._searchPositionsService = searchPositionsService;
            this._dataCache = new DataCacheHelper(options);
            this._profileDataGatherer = profileDataGatherer;
        }

        // GET: api/GeneralFilter/employees
        [HttpGet]
        [Route("employees")]
        //[Authorize(Roles = "HrDepartment")]
        [Authorize(Roles = "Employee,HrFeedbackAdmin,LineManager,TeamLeader")]
        public async Task<List<EmployeeEntity>> GetEmployees()
        {
            var employees = await _dataCache.GetAll<EmployeeEntity>();
                       
            return employees;
        }
        //Get : api/GeneralFilter/employees/{id}
        [HttpGet("{id}")]
        [Route("employee/{id}")]
        [Authorize(Roles = "Employee,HrFeedbackAdmin,LineManager,TeamLeader")]
        public async Task<EmployeeEntity> GetEmployeeById(string id)
        {
            var employees = await _dataCache.GetAll<EmployeeEntity>();
            var employee = employees.Where(x => x.Id == id).FirstOrDefault();
            
            return employee;
        }
        // GET: api/GeneralFilter/filteredEmployees/ana
        [HttpGet("{filter}")]
        [Route("filteredEmployees/{filter}")]
        [Authorize(Roles = "Employee,HrFeedbackAdmin,LineManager,TeamLeader")]
        public async Task<IEnumerable<EmployeeEntity>> GetEmployeesFiltered(string filter)
        {
            var employees = await _searchEmployeesService.GetEmployeesFiltered(filter);

            return employees;
        }

        // GET: api/GeneralFilter/units
        [HttpGet]
        [Route("units")]
        [Authorize(Roles = "Employee,HrFeedbackAdmin,LineManager,TeamLeader")]
        public async Task<List<UnitEntity>> GetUnits()
        {
            var units = await _dataCache.GetAll<UnitEntity>();

            return units;
        }

        // GET: api/GeneralFilter/units/13
        [HttpGet("{id}")]
        [Route("units/{id}")]
        [Authorize(Roles = "Employee,HrFeedbackAdmin,LineManager,TeamLeader")]
        public async Task<UnitEntity> GetUnitById(string id)
        {
            var units = await _dataCache.GetAll<UnitEntity>();
            var unit = units.Where(x => x.Id == id).FirstOrDefault();

            return unit;
        }

        // GET: api/GeneralFilter/teams
        [HttpGet]
        [Route("teams")]
        [Authorize(Roles = "Employee,HrFeedbackAdmin,LineManager,TeamLeader")]
        public async Task<List<TeamEntity>> GetTeams()
        {
            var teams = await _dataCache.GetAll<TeamEntity>();

            return teams;
        }

        // GET: api/GeneralFilter/teams/1
        [HttpGet("{id}")]
        [Route("teams/{id}")]
        [Authorize(Roles = "Employee,HrFeedbackAdmin,LineManager,TeamLeader")]
        public async Task<TeamEntity> GetTeamById(string id)
        {
            var teams = await _dataCache.GetAll<TeamEntity>();
            var team = teams.Where(x => x.Id == id).FirstOrDefault();

            return team;

        }

        // GET: api/GeneralFilter/locations
        [HttpGet]
        [Route("locations")]
        [Authorize(Roles = "Employee,HrFeedbackAdmin,LineManager,TeamLeader")]
        public async Task<List<LocationEntity>> GetLocations()
        {
            var locations = await _dataCache.GetAll<LocationEntity>();
            return locations;
        }

        // GET: api/GeneralFilter/locations/1
        [HttpGet("{id}")]
        [Route("locations/{id}")]
        [Authorize(Roles = "Employee,HrFeedbackAdmin,LineManager,TeamLeader")]
        public async Task<LocationEntity> GetLocationById(string id)
        {
            var locations = await _dataCache.GetAll<LocationEntity>();
            var location = locations.Where(x => x.Id == id).FirstOrDefault();

            return location;

        }

        // GET: api/GeneralFilter/positions
        [HttpGet]
        [Route("positions")]
        [Authorize(Roles = "Employee,HrFeedbackAdmin,LineManager,TeamLeader")]
        public async Task<List<PositionEntity>> GetPositions()
        {
            var positions = await _dataCache.GetAll<PositionEntity>();
            return positions;
        }

        // GET: api/GeneralFilter/positions/1
        [HttpGet("{id}")]
        [Route("positions/{id}")]
        [Authorize(Roles = "Employee,HrFeedbackAdmin,LineManager,TeamLeader")]
        public async Task<PositionEntity> GetPositionById(string id)
        {
            var positions = await _dataCache.GetAll<PositionEntity>();
            var position = positions.Where(x => x.Id == id).FirstOrDefault();

            return position;

        }
        [HttpGet("{id}")]
        [Route("teamLead/{id}")]
        [Authorize(Roles = "Employee,HrFeedbackAdmin,LineManager,TeamLeader")]
        public async Task<EmployeeEntity> GetTeamLeadByEmployeeId(string id)
        {
            var employees = await _dataCache.GetAll<EmployeeEntity>();
            var employee = employees.Where(x => x.Id == id).FirstOrDefault();
            EmployeeEntity teamLead = null;
            if (employee.TeamId != null)
            {
                var teams = await _dataCache.GetAll<TeamEntity>();
                var team = teams.Where(x => x.Id == employee.TeamId).FirstOrDefault();
                if (team != null && team.TeamLeadId != null)
                {
                    teamLead = employees.Where(x => x.Id == team.TeamLeadId).FirstOrDefault();
                }
                else
                {
                    teamLead = new EmployeeEntity();
                    teamLead.FirstName = "undefined";
                    teamLead.LastName = " ";
                }
            }
            else
            {
                teamLead = new EmployeeEntity();
                teamLead.FirstName = "undefined";
                teamLead.LastName = " ";
            }
            return teamLead;
        }

        [HttpGet("{id}")]
        [Route("lineManager/{id}")]
        [Authorize(Roles = "Employee,HrFeedbackAdmin,LineManager,TeamLeader")]
        public async Task<EmployeeEntity> GetLineManagerByEmployeeId(string id)
        {
            var employees = await _dataCache.GetAll<EmployeeEntity>();
            var employee = employees.Where(x => x.Id == id).FirstOrDefault();
            EmployeeEntity lineManager = null;
            if (employee.UnitId != null)
            {
                var units = await _dataCache.GetAll<UnitEntity>();
                //var unit = units.Where(x => x.Id == employee.UnitId).FirstOrDefault();
                if (employee.UnitId != "0")
                {
                    lineManager = employees.Where(x => x.UnitId == employee.UnitId && x.IsLineManager == true).FirstOrDefault();
                    // lineManager = employees.Where(x => x.Id == unit.LineManagerId).FirstOrDefault();
                }
                else
                {
                    lineManager = new EmployeeEntity();
                    lineManager.FirstName = "undefined";
                }

            }
            else
            {
                lineManager = new EmployeeEntity();
                lineManager.FirstName = "undefined";
            }
            return lineManager;
        }

        [HttpGet("{id}")]
        [Route("years/{id}")]
        public async Task<int> GetYearsByEmployeeId(string id)
        {
            var employees = await _dataCache.GetAll<EmployeeEntity>();
            var employee = employees.Where(x => x.Id == id).FirstOrDefault();
            string pattern = "dd.MM.yyyy";
            DateTime employmentDate;
            var years = 0;
            if (DateTime.TryParseExact(employee.EmploymentDate, pattern, null, DateTimeStyles.None, out employmentDate))
            {
                years = DateTime.Now.Year - employmentDate.Year;
            }
             
            return years;
        }

        [HttpGet("{dateValue}")]
        [Route("getDate/{dateValue}")]
        public DateTime GetEndEOPDate(string dateValue)
        {
            string pattern = "dd.MM.yyyy";
            DateTime employmentDate;
            string dateString=null;
            DateTime end = new DateTime();
            if (DateTime.TryParseExact(dateValue, pattern, null, DateTimeStyles.None, out employmentDate))
            {
                var endDate = employmentDate.AddMonths(3);
                dateString = endDate.Day + "." + endDate.Month + "." + endDate.Year;
                end = new DateTime(endDate.Year, endDate.Month, endDate.Day);
            }
            //return dateString;
            return end;
        }

        [HttpGet("{dateValue}")]
        [Route("getStartDate/{dateValue}")]
        public DateTime GetStartEOPDate(string dateValue)
        {
            var day = int.Parse(dateValue.Split(".")[0]);
            var month = int.Parse(dateValue.Split(".")[1]);
            var year = int.Parse(dateValue.Split(".")[2]);
           
            return new DateTime(year, month, day);
           
        }

        [HttpPost]
        [Route("getEmployeeDocuments")]
        public async Task<List<ProfileFormModel>> GetEmployeeDocuments([FromBody]ProfileFormFilter filter)
        {
            var employeeDocs = await _profileDataGatherer.GetAllFormsForEmployee(filter.EmployeeId);

            if(filter.Year > 0)
            {
               employeeDocs =  employeeDocs.Where(doc => doc.YearOfForm == filter.Year).ToList();
            }

            if(filter.TalkType > 0)
            {
                employeeDocs = employeeDocs.Where(doc => doc.TalkType == filter.TalkType).ToList();
            }
               
            if(filter.HasFollowUp.HasValue)
            {
                employeeDocs = employeeDocs.Where(doc => doc.IsOpenForFollowUp == filter.HasFollowUp.Value).ToList();
            }

            return employeeDocs;
        }
    }
}
