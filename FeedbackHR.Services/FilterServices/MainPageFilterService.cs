﻿using FeedbackHR.Entities.Filter;
using FeedbackHR.ServiceEntities.ServiceModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackHR.Services.FilterServices
{
    public class MainPageFilterService
    {
        public MainPageFilterService()
        {

        }
        public List<EmployeeFormModel> GetFiltered(List<EmployeeFormModel> forms, EmployeeFormFilter filter)
        {
            List<EmployeeFormModel> filtered = forms;

            try
            {
                if (!string.IsNullOrEmpty(filter.LocationId))
                    filtered = filtered.Where(x => x.LocationId == filter.LocationId).ToList();
                if (!string.IsNullOrEmpty(filter.UnitId))
                    filtered = filtered.Where(x => x.UnitId == filter.UnitId).ToList();
                if (!string.IsNullOrEmpty(filter.TeamId))
                    filtered = filtered.Where(x => x.TeamId == filter.TeamId).ToList();
                if (filter.YearsFrom > 0)
                    filtered = filtered.Where(x => x.YearsWorking >= filter.YearsFrom).ToList();
                if (filter.YearsTo > 0)
                    filtered = filtered.Where(x => x.YearsWorking <= filter.YearsTo).ToList();
                if (filter.YearOfForm > 0)
                    filtered = filtered.Where(x => x.TalkDate.Year == filter.YearOfForm).ToList();
                if (filter.Type > 0)
                    filtered = filtered.Where(x => x.TalkType == filter.Type).ToList();
                if (filter.HasFollowUp == true || filter.HasFollowUp == false)
                    filtered = filtered.Where(x => x.IsOpenForFollowUp == filter.HasFollowUp).ToList();
            }
            catch (Exception)
            {
                throw;
            }
            return filtered;
        }

    }
}
