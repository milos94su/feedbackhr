﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FeedbackHR.Entities;
using FeedbackHR.Entities.Filter;
using FeedbackHR.Infrastructure.Cache;
using FeedbackHR.Repository;
using FeedbackHR.ServiceEntities.ServiceModels;
using FeedbackHR.Services.DataHelpService;
using FeedbackHR.Services.FilterServices;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace FeedbackHR.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentFilterController : ControllerBase
    {
        private MainPageFilterService _filterService;
        private DataMapper _dataMapper;

        public DocumentFilterController(IOptions<CacheSettings> options,
            IRepository<EndOfProbationFormEntity> eopRepository,
            IRepository<ExitFormEntity> exitRepository,
            IRepository<PerformanceAppraisalTalkFormEntity> patRepository,
            IRepository<FollowUpAdHocFormEntity> fupRepository)
        {
            _filterService = new MainPageFilterService();
            _dataMapper = new DataMapper(options, eopRepository, exitRepository, patRepository, fupRepository);
        }

        // GET: api/DocumentFilter/forms
        [HttpGet]
        [Route("forms")]
        public async Task<List<EmployeeFormModel>> GetAll()
        {
            return await _dataMapper.GetAll();            
        }

        // GET: api/GeneralFilter/filteredforms
        [HttpPost]
        [Route("filteredforms")]
        public async Task<List<EmployeeFormModel>> GetFiltered([FromBody]EmployeeFormFilter filter)
        {
            List<EmployeeFormModel> forms = await GetAll();
            
            return  _filterService.GetFiltered(forms, filter);
        }

        [HttpGet]
        [Route("unresolvedforms")]
        public async Task<List<EmployeeFormModel>> GetAllUnresolvedForms()
        {
            return (await _dataMapper.GetAll()).Where(x => x.IsOpenForFollowUp == true).OrderByDescending(x => x.FollowUpDate).ToList();
        }
    }
}