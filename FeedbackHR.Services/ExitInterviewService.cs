﻿using FeedbackHR.Entities;
using FeedbackHR.Repository;
using FeedbackHR.ServiceEntities;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackHR.Services
{
    public class ExitInterviewService : IExitInterviewService
    {
        private IRepository<ExitFormEntity> _repository;
        private IValidator<ExitFormEntity> _validator;

        public ExitInterviewService(IRepository<ExitFormEntity> repository, IValidator<ExitFormEntity> validator)
        {
            this._repository = repository;
            this._validator = validator;
        }        

        public async Task<IEnumerable<ExitFormEntity>> GetAllAsync()
        {
           var entities = await _repository.GetAllAsync();

            return entities;
        }

        public async Task<ExitFormEntity> GetByIdAsync(string id)
        {
            var exitForm = await _repository.GetByIdAsync(id);

            return exitForm;
        }

        public async Task<bool> UpdateAsync(ExitFormEntity entity)
        {
            var valid = _validator.Validate(entity).IsValid;

            if (!valid)
            {
                var errors = _validator.Validate(entity).Errors;
                throw new ValidationException("Entity is not valid!", errors);
                
            }

            var updated = await _repository.UpdateAsync(entity);
            return updated;
        }

        public async Task<bool> DeleteByIdAsync(string id)
        {
            var isDeleted = await _repository.DeleteByIdAsync(id);

            return isDeleted;
        }
    }
}
