﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeedbackHR.Entities
{ 
    public enum FollowUpOptionsName
    {
        ProjectAndTechnologies = 1,
        WorkLoadAndWorkOrganization,
        TasksAndContributionToTheProject,
        RelationshipWithClientsOrEndUsers,
        TeamCooperation,
        CooperationWithTL,
        CooperationWithLM,
        OverallSatisfactionWithCompanyAndManagement,
        InternalCommunicationAndCompanyPolicies,
        PhysicalWorkingEnvironment,
        CompanyOrJobStability,
        TeamBuildingActivities,
        OpportunityForProffesionalAndCareerDevelopment,
        Benefits,
        Salary
    }

    public class FollowUpTypeEntity : IFeedbackHREntity
    {
        public string Id { get; set; }
        public FollowUpOptionsName FollowUpOption { get; set; }  //this is FollowUpOptionName
        public bool Checked { get; set; }
        public FollowUpTypeEntity()
        {
            Id = ObjectIdFactory.NewId().ToString();
        }
    }
}
