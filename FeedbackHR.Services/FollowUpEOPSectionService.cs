﻿using FeedbackHR.Entities;
using FeedbackHR.Infrastructure.Validation;
using FeedbackHR.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using FeedbackHR.ServiceEntities;

namespace FeedbackHR.Services
{
     public class FollowUpEOPSectionService : IFollowUpEOPSectionService
    {
        private IRepository<EndOfProbationFormEntity> _repository;
        private FollowUpTypeEntityValidator _validator;
        public FollowUpEOPSectionService(IRepository<EndOfProbationFormEntity> repository, FollowUpTypeEntityValidator validator)
        {
             
            _repository = repository;
            _validator = validator;
        }
        private bool IsValid(FollowUpTypeEntity entity)
        {
            var isValidate = _validator.Validate(entity).IsValid;  
            if (!isValidate)
            {
                return false;
            }
            else return true;
        }
      
        public async Task<FollowUpSectionEntity> GetByIdAsync(string EOPId)
        {
            var EOP = await _repository.GetByIdAsync(EOPId);
            var Section = EOP.FollowUpSection;
            return Section;
        }

        public async Task<bool> UpdateAsync(EndOfProbationFormEntity eop)
        {
            var FollowUpTypes = eop.FollowUpSection.FollowUpOptions;

            foreach (var FollowUp in FollowUpTypes)
            {
                var isValid = IsValid(FollowUp);
                if (isValid)
                {
                    //Creating
                    var concrete = new FollowUpConcreteEntity();
                    concrete.FollowUpEntityId = FollowUp.Id;
                    eop.FollowUpsToDo.Add(concrete);

                    if (FollowUp.FollowUpOption ==FollowUpOptionsName.Salary)
                    {
                        concrete.SalaryAfter = new SalaryEntity();
                    }
                }
                else
                    throw new Exception("Follow Up is not valid");
            }
            if (eop.FollowUpsToDo.Count != 0 && eop.FollowUpsToDo.Where(x=>x.Resolved==true) !=null)
            {
                eop.IsOpenedForFollowUp = true;
            }
            else 
                eop.IsOpenedForFollowUp = false;
      
            var FollowUpOptions = eop.FollowUpSection.FollowUpOptions;
            if (FollowUpOptions != null)
            {
                return await _repository.UpdateAsync(eop);
            }
            else
            {                
                throw new ArgumentNullException("FollowUpOptions property of FollowUpSection is null");
            }
        }
    }
}
