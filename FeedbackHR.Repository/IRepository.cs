﻿using FeedbackHR.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedbackHR.Repository
{
    public interface IRepository<TEntity> : IWriteRepository<TEntity>, IReadRepository<TEntity>, IDeleteRepository<TEntity> where TEntity : IFeedbackHREntity
    {
    }
}
