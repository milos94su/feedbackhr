﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeedbackHR.Entities
{
    public class FollowUpConcreteEntity : IFeedbackHREntity
    {
        public string Id { get; set; }
        public string FollowUpEntityId { get; set; }
        public bool Resolved { get; set; }
        public string ShortComment { get; set; }
        public SalaryEntity SalaryAfter { get; set; }  //If FollowUpEntityId is Id of salary its initialized, otherwise its null

        public FollowUpConcreteEntity()
        {
            Id = ObjectIdFactory.NewId().ToString();
        }
    }
}
