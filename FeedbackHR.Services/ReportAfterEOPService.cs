﻿using FeedbackHR.Entities;
using FeedbackHR.Infrastructure.Validation;
using FeedbackHR.Repository;
using FeedbackHR.ServiceEntities.ReportEOPService;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackHR.Services
{
    public class ReportAfterEOPService : IReportAfterEOPService
    {
        private IRepository<EndOfProbationFormEntity> _repository;
        private ReportAfterEOPEntityValidator _validator;

        public ReportAfterEOPService(IRepository<EndOfProbationFormEntity> repository, ReportAfterEOPEntityValidator validator)
        {
            _repository = repository;
            _validator = validator;
        }
        private bool IsValid(ReportAfterEOPEntity entity)
        {
            var isValidate = _validator.Validate(entity).IsValid;  // Create StartDate get from FMR database API, EndDate StartDate +3 month (recomended) or insert manual
            if (!isValidate)
            {
                return false;
            }
            else return true;
        }


        public async Task<ReportAfterEOPEntity> GetByEndOfProbationIdAsync(string EOPid) 
        {
            var EOP = await _repository.GetByIdAsync(EOPid);
            var EOPReport = EOP.ReportEOP;
            return EOPReport;
        }

        public async Task<bool> UpdateAsync(ReportAfterEOPEntity ReportEOPEntity, string EOPid)
        {
            var isValid = IsValid(ReportEOPEntity);
            if (isValid)
            {
                var EOP = await _repository.GetByIdAsync(EOPid);
                EOP.ReportEOP = ReportEOPEntity;
                var result = await _repository.UpdateAsync(EOP);
                return result;
            }
            else
            {
                throw new ArgumentException("ReportEOP is not valid");
            }
        }
    }
}
