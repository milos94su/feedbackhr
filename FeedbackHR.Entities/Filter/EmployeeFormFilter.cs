﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeedbackHR.Entities.Filter
{
    public class EmployeeFormFilter : IFeedbackHRFilter
    {
        public string LocationId { get; set; }
        public string TeamId { get; set; }
        public string UnitId { get; set; }
        public int YearsFrom { get; set; }
        public int YearsTo { get; set; }
        public FormType Type { get; set; }
        public int YearOfForm { get; set; }
        public bool? HasFollowUp { get; set; }
    }
}
