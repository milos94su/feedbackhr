﻿using FeedbackHR.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedbackHR.ServiceEntities.ServiceModels
{
    public class ProfileFormModel
    {
        public FormType TalkType { get; set; }
        public string FormId { get; set; }
        public string EmployeeId { get; set; }
        public DateTime DateOfTalk { get; set; }
        public int YearOfForm { get; set; }
        public bool IsOpenForFollowUp { get; set; }
        public DateTime? FollowUpDate { get; set; }
    }
}
