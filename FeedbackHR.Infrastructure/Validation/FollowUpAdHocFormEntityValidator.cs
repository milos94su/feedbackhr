﻿using System;
using System.Collections.Generic;
using System.Text;
using FeedbackHR.Entities;
using FluentValidation;

namespace FeedbackHR.Infrastructure.Validation
{
    public class FollowUpAdHocFormEntityValidator : AbstractValidator<FollowUpAdHocFormEntity>
    {
        public FollowUpAdHocFormEntityValidator()
        {
            RuleFor(x => x.DateOfTalk).NotEmpty().WithMessage("*Required");
            RuleFor(x => x.FormType).Equal(FormType.AdHocMeeting).WithMessage("Form type must be Ad Hoc Meeting");
        }
    }
}
