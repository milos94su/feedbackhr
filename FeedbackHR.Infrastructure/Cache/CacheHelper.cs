﻿using FeedbackHR.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedbackHR.Infrastructure.Cache
{
    public static class CacheHelper
    {
        public static Domains GetDomainByEntity(object entity)
        {

            switch (entity)
            {
                case EmployeeEntity e:
                    return Domains.Employees;
                case TeamEntity t:
                    return Domains.Teams;
                case UnitEntity u:
                    return Domains.Units;
                case LocationEntity l:
                    return Domains.Locations;
                case PositionEntity p:
                    return Domains.HrPositions;
                case EndOfProbationFormEntity eop:
                    return Domains.EndOfProbations;
                case ExitFormEntity exit:
                    return Domains.ExitInterviews;
                case PerformanceAppraisalTalkFormEntity pat:
                    return Domains.PerformanceAppraisalTalks;
                case FollowUpAdHocFormEntity adHoc:
                    return Domains.AdHocFollowUps;
                case FollowUpTypeEntity followUpType:
                    return Domains.FollowUpOptions;
                case null:
                    throw new ArgumentNullException(entity.GetType().Name, "GetDomainEntity cannot proccess null value");
                default:
                    throw new ArgumentException("Method doesn't expect this type", entity.GetType().Name);

            }
        }

        public static Domains GetDomainByType(Type t)
        {
            if (t == null)
                throw new ArgumentNullException();
            if (t == typeof(EmployeeEntity))
                return Domains.Employees;
            if (t == typeof(TeamEntity))
                return Domains.Teams;
            if (t == typeof(UnitEntity))
                return Domains.Units;
            if (t == typeof(LocationEntity))
                return Domains.Locations;
            if (t == typeof(PositionEntity))
                return Domains.HrPositions;
            if (t == typeof(EndOfProbationFormEntity))
                return Domains.EndOfProbations;
            if (t == typeof(ExitFormEntity))
                return Domains.ExitInterviews;
            if (t == typeof(PerformanceAppraisalTalkFormEntity))
                return Domains.PerformanceAppraisalTalks;
            if (t == typeof(FollowUpAdHocFormEntity))
                return Domains.AdHocFollowUps;
            if (t == typeof(FollowUpTypeEntity))
                return Domains.FollowUpOptions;
            return Domains.Default;

        }
    }
}
