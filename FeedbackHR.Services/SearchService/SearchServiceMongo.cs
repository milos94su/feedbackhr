﻿using FeedbackHR.Entities;
using FeedbackHR.Repository;
using FeedbackHR.ServiceEntities.SearchService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackHR.Services.SearchService
{
    public class SearchServiceMongo<TEntity> : ISearchServiceMongo<TEntity> where TEntity : IFeedbackHREntity
    {
        private readonly IReadRepository<TEntity> _repository;

        public SearchServiceMongo(IReadRepository<TEntity> repository)
        {
            _repository = repository;
        }

        public async Task<List<TEntity>> GetAll()
        {
            return (await _repository.GetAllAsync()).ToList();
        }

        public async Task<TEntity> GetById(string Id)
        {
            return await _repository.GetByIdAsync(Id);
        }
    }
}
