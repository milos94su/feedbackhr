﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeedbackHR.Entities
{
    public class UserEntity : IFeedbackHREntity
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string HashPassword { get; set; }
        public string Email { get; set; }
        public string EmployeeId { get; set; }
        public List<RoleEntity> Roles { get; set; }

        public UserEntity()
        {
            Id = ObjectIdFactory.NewId().ToString();
        }
    }
}
