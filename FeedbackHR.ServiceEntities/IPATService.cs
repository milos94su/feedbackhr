﻿using FeedbackHR.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackHR.ServiceEntities
{
    public interface IPATService
    {

        Task<IEnumerable<PerformanceAppraisalTalkFormEntity>> GetAllAsync();
        Task<PerformanceAppraisalTalkFormEntity> GetByIdAsync(string PATId);
        Task<bool> DeleteAsync(string PATId);
        Task<bool> UpdateAsync(PerformanceAppraisalTalkFormEntity entity);
    }
}
