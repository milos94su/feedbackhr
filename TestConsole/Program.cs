﻿using FeedbackHR.DAL;
using FeedbackHR.DAL.Repository;
using FeedbackHR.Entities;
using FeedbackHR.IoC;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using System;
using System.IO;
using FeedbackHR.DAL.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using FeedbackHR.Services;


namespace TestConsole
{
   
    class Program
    {
        static async Task Main(string[] args)
        {
            var provider = BuildServices();

            var x=await Helper<EmployeeEntity>.GetAllAsync();

            using (var scope = provider.CreateScope())
            {
               

                System.Console.ReadLine();
            }

        }


        private static IServiceProvider BuildServices()
        {
            var services = new ServiceCollection();
            var configuration = BuildConfiguration();

            services.Configure<DbSettings>(configuration.GetSection("DbSettings"));
            services.Configure<Api>(configuration.GetSection("Api"));

            
            services.AddTransient<IMongoDatabase>(sp => {
                return sp.GetMongoDatabase();
            }
              );
            var api = configuration.GetSection("Api").Get<Api>();
            Helper<EmployeeEntity>.Run(api);
            Helper<TeamEntity>.Run(api);
            Helper<UnitEntity>.Run(api);
            Helper<LocationEntity>.Run(api);
            Helper<PositionEntity>.Run(api);
          

            ServiceRegistration.RegisterServices(services, configuration);
            return services.BuildServiceProvider();
        }

        private static IConfiguration BuildConfiguration()
        {
            var file = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
            return new ConfigurationBuilder()
                .AddJsonFile(file, optional: false, reloadOnChange: true)
                .Build();
        }
    }
}
