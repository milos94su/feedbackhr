﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeedbackHR.Entities
{
    public static class ObjectIdFactory
    {
        public static Func<string> NewId { get; set; } = () => Guid.NewGuid().ToString();
    }
}
