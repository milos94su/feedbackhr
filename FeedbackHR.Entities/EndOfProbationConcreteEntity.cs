﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeedbackHR.Entities
{
    public class EndOfProbationConcreteEntity : IFeedbackHREntity
    {
        public string Id { get; set; }

        public PerformanceFactorEntity Quality { get; set; }
        public PerformanceFactorEntity Productivity { get; set; }
        public PerformanceFactorEntity ProfessionalKnowledge { get; set; }
        public PerformanceFactorEntity Reliability { get; set; }
        public PerformanceFactorEntity Communication { get; set; }
        public PerformanceFactorEntity TeamWorkAndCooperation { get; set; }
        public PerformanceFactorEntity Autonomy { get; set; }

        public string OverallImpression { get; set; }
        public string ApprasierComment { get; set; }

        public EndOfProbationConcreteEntity()
        {
            Id = ObjectIdFactory.NewId().ToString();
        }
    }
}
