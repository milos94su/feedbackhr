﻿using FeedbackHR.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackHR.ServiceEntities
{
    public interface IFinalConcreteService
    {
        Task<PerformanceFactorEntity[][]> CreateFinalEOPMatrix(string EOPid);
        Task<string[][]> CreateFinalEOPComments(string EOPid);
        Task<PerformanceFactorEntity[][]> CreateFinalPATMatrix(string PATid);
        Task<string[][]> CreateFinalPATComments(string PATid);
    }
}
