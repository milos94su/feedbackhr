﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using FeedbackHR.ServiceEntities;
using FeedbackHR.Entities;
using FluentValidation;
using FeedbackHR.Repository;

namespace FeedbackHR.Services
{
    public class CreateService<TEntity> : ICreateService<TEntity> where TEntity : IFeedbackHREntity
    {
        private readonly IValidator<TEntity> _validator;
        private readonly IWriteRepository<TEntity> _repository;
        public CreateService(IValidator<TEntity> validator, IRepository<TEntity> repository)
        {
            _validator = validator;
            _repository = repository;
        }
        public async Task<bool> CreateAsync(TEntity entity)
        {
            var IsValid = _validator.Validate(entity).IsValid;

            if (IsValid == true)
            {
                var result = await _repository.InsertAsync(entity);
                return result;
            }
            else
            {
                IEnumerable<FluentValidation.Results.ValidationFailure> errors = _validator.Validate(entity).Errors;
                throw new ValidationException("Entity is not valid!", errors);
            }
        }
        public void InstanceEOP(EndOfProbationFormEntity entity)
        {
            entity.Salary = new SalaryEntity() { IsSatisfied = true };
            entity.FollowUpSection = new FollowUpSectionEntity();
            entity.FollowUpsToDo = new List<FollowUpConcreteEntity>();
            entity.ClientEOP = new EndOfProbationConcreteEntity();
            entity.TeamLeadEOP = new EndOfProbationConcreteEntity();
            entity.LineManagerEOP = new EndOfProbationConcreteEntity();

            var reportEop = new ReportAfterEOPEntity {StartDate = entity.ReportEOP.StartDate, EndDate = entity.ReportEOP.EndDate};
            entity.ReportEOP = reportEop;
            entity.FollowUpSection = new FollowUpSectionEntity();
            entity.FollowUpSection.FollowUpOptions = new List<FollowUpTypeEntity>();

            for (int i=1; i <= 15; i++) 
            {
                var followUpType = new FollowUpTypeEntity { FollowUpOption = (FollowUpOptionsName)i };
                entity.FollowUpSection.FollowUpOptions.Add(followUpType);
            }

            InstanceEOPFollowUpsToDo(entity);
        }
        public void InstanceEOPFollowUpsToDo(EndOfProbationFormEntity entity)
        {
            entity.FollowUpsToDo = new List<FollowUpConcreteEntity>();
            var FollowUpTypes = entity.FollowUpSection.FollowUpOptions;

            foreach (var followUp in FollowUpTypes)
            {
                //Creating
                var concreteFollowUp = new FollowUpConcreteEntity();
                concreteFollowUp.FollowUpEntityId = followUp.Id;
                concreteFollowUp.Resolved = false;
                if (followUp.FollowUpOption == FollowUpOptionsName.Salary)
                    concreteFollowUp.SalaryAfter = new SalaryEntity() { IsSatisfied = true };

                entity.FollowUpsToDo.Add(concreteFollowUp);
            }
        }

        public void InstancePAT(PerformanceAppraisalTalkFormEntity entity)
        {
            entity.Salary = new SalaryEntity() { IsSatisfied = true};
            entity.FollowUpSection = new FollowUpSectionEntity();
            entity.FollowUpsToDo = new List<FollowUpConcreteEntity>();
            entity.ClientPAT = new PerformanceAppraisalTalkConcreteEntity();
            entity.TeamLeadPAT = new PerformanceAppraisalTalkConcreteEntity();
            entity.LineManagerPAT = new PerformanceAppraisalTalkConcreteEntity();
            entity.FollowUpSection = new FollowUpSectionEntity();
            entity.FollowUpSection.FollowUpOptions = new List<FollowUpTypeEntity>();

            for (int i = 1; i <= 15; i++)
            {
                var followUpType = new FollowUpTypeEntity { FollowUpOption = (FollowUpOptionsName)i };
                entity.FollowUpSection.FollowUpOptions.Add(followUpType);
            }

            InstancePATFollowUpsToDo(entity);
        }

        public void InstancePATFollowUpsToDo(PerformanceAppraisalTalkFormEntity entity)
        {
            entity.FollowUpsToDo = new List<FollowUpConcreteEntity>();
            var FollowUpTypes = entity.FollowUpSection.FollowUpOptions;

            foreach (var followUp in FollowUpTypes)
            {
                //Creating
                var concreteFollowUp = new FollowUpConcreteEntity();
                concreteFollowUp.FollowUpEntityId = followUp.Id;
                concreteFollowUp.Resolved = false;
                if (followUp.FollowUpOption == FollowUpOptionsName.Salary)
                    concreteFollowUp.SalaryAfter = new SalaryEntity() { IsSatisfied = true};

                entity.FollowUpsToDo.Add(concreteFollowUp);
            }
        }
    }
}
