﻿using FeedbackHR.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackHR.ServiceEntities.SearchService.SearchEmployeesService
{
   public interface ISearchEmployeesService
    {
        Task<List<EmployeeEntity>> GetEmployees();
        Task<List<EmployeeEntity>> GetEmployeesFiltered(string entity);
        Task<EmployeeEntity> GetEmployeeById(string id);

    }
}
