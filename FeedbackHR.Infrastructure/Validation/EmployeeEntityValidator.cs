﻿using FeedbackHR.Entities;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedbackHR.Infrastructure.Validation
{
    public class EmployeeEntityValidator : AbstractValidator<EmployeeEntity>
    {
        public EmployeeEntityValidator()
        {
            RuleFor(x => x.FirstName).NotNull().NotEmpty().WithMessage("First name must not be empty");
            RuleFor(x => x.LastName).NotNull().NotEmpty().WithMessage("Last name must not be empty");
            RuleFor(x=>x.EmploymentDate).NotNull().NotEmpty().WithMessage("Date of employment must not be empty");
            RuleFor(x => x.BirthDate).NotNull().NotEmpty().WithMessage("Date of birth must not be empty");
            RuleFor(x => x.LocationId).NotNull().NotEmpty().WithMessage("Location must not be empty");
        }
    }
}
