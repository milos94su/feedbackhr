﻿using FeedbackHR.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FeedbackHR.Repository
{
    public interface IDeleteRepository<TEntity> where TEntity : IFeedbackHREntity
    {
        Task<bool> DeleteByIdAsync(string id, CancellationToken cancellationToken = default);
    }
}
