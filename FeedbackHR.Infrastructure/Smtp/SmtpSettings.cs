﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeedbackHR.Infrastructure.Smtp
{
    public class SmtpSettings
    {
        public string SmtpClientHost { get; set; }
        public string SmtpClientPort { get; set; }
        public string NetworkCredentialUserName { get; set; }
        public string NetworkCredentialPassword { get; set; }
    }
}
