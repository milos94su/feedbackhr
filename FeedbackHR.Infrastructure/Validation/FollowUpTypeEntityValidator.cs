﻿using System;
using System.Collections.Generic;
using System.Text;
using FeedbackHR.Entities;
using FluentValidation;

namespace FeedbackHR.Infrastructure.Validation
{
    public class FollowUpTypeEntityValidator : AbstractValidator<FollowUpTypeEntity>
    {
        public FollowUpTypeEntityValidator()
        {
            RuleFor(x => x.FollowUpOption).NotNull().NotEmpty().WithMessage("Follow up option must not be empty") ;
        }
    }
}
