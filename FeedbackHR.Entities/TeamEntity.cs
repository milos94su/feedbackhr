﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeedbackHR.Entities
{
    public class TeamEntity : IFeedbackHREntity
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string TeamLeadId { get; set; }
        public string UnitId { get; set; }
        public List<EmployeeEntity> TeamEmployees { get; set; }
        
    }
}
