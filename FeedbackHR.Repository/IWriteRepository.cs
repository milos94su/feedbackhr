﻿using FeedbackHR.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FeedbackHR.Repository
{
    public interface IWriteRepository<TEntity> where TEntity : IFeedbackHREntity
    {
        Task<bool> InsertAsync(TEntity entity, CancellationToken cancellationToken = default);
        Task<bool> UpdateAsync(TEntity entity, CancellationToken cancellationToken = default);
    }
}
