﻿using FeedbackHR.Entities;
using FeedbackHR.Infrastructure.Cache;
using FeedbackHR.Repository;
using FeedbackHR.ServiceEntities;
using FeedbackHR.ServiceEntities.ServiceModels;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackHR.Services.DataHelpService
{
    public class ProfileDataGatherer : IProfileDataGatherer
    {
        private readonly IRepository<EndOfProbationFormEntity> _eopRepository;
        private readonly IRepository<PerformanceAppraisalTalkFormEntity> _patRepository;
        private readonly IRepository<FollowUpAdHocFormEntity> _fupRepository;
        private readonly IRepository<ExitFormEntity> _exitRepository;

        public ProfileDataGatherer(
            IRepository<EndOfProbationFormEntity> eopRepository,
            IRepository<PerformanceAppraisalTalkFormEntity> patRepository,
            IRepository<FollowUpAdHocFormEntity> fupRepository,
            IRepository<ExitFormEntity> exitRepository
            )
        {
            
            _eopRepository = eopRepository;
            _patRepository = patRepository;
            _fupRepository = fupRepository;
            _exitRepository = exitRepository;
        }

        public async Task<List<ProfileFormModel>> GetAllFormsForEmployee(string employeeId)
        {
            var collection = new List<ProfileFormModel>();

            collection.AddRange(await GetEopsForEmployee(employeeId));
            collection.AddRange(await GetPatsForEmployee(employeeId));
            collection.AddRange(await GetExitForEmployee(employeeId));
            collection.AddRange(await GetFollowUpsForEmployee(employeeId));

            return collection.OrderByDescending(x => x.DateOfTalk).ToList();

        }

        private async Task<List<ProfileFormModel>> GetEopsForEmployee(string employeeId)
        {
            var eopCollection = await _eopRepository.GetAllAsync() as List<EndOfProbationFormEntity>;

            var query = from eop in eopCollection
                        where eop.EmployeeId == employeeId
                        select new ProfileFormModel
                        {
                            TalkType = eop.FormType,
                            FormId = eop.Id, 
                            EmployeeId = eop.EmployeeId,
                            DateOfTalk = eop.DateOfTalk,
                            YearOfForm = eop.DateOfTalk.Year,
                            IsOpenForFollowUp = eop.IsOpenedForFollowUp,
                            FollowUpDate = eop.FollowUpSection.TalkDate
                        };

            return query.ToList();
        }

        private async Task<List<ProfileFormModel>> GetPatsForEmployee(string employeeId)
        {
            var patCollection = await _patRepository.GetAllAsync() as List<PerformanceAppraisalTalkFormEntity>;

            var query = from pat in patCollection
                        where pat.EmployeeId == employeeId
                        select new ProfileFormModel
                        {
                            TalkType = pat.FormType,
                            FormId = pat.Id,
                            EmployeeId = pat.EmployeeId,
                            DateOfTalk = pat.DateOfTalk,
                            YearOfForm = pat.DateOfTalk.Year,
                            IsOpenForFollowUp = pat.IsOpenedForFollowUp,
                            FollowUpDate = pat.FollowUpSection.TalkDate
                        };

            return query.ToList();
        }

        private async Task<List<ProfileFormModel>> GetExitForEmployee(string employeeId)
        {
            var exitCollection = await _exitRepository.GetAllAsync() as List<ExitFormEntity>;

            var query = from exit in exitCollection
                        where exit.EmployeeId == employeeId
                        select new ProfileFormModel
                        {
                            TalkType = exit.FormType,
                            FormId = exit.Id,
                            EmployeeId = exit.EmployeeId,
                            DateOfTalk = exit.DateOfTalk,
                            YearOfForm = exit.DateOfTalk.Year,
                            IsOpenForFollowUp = false,
                            FollowUpDate = null
                        };

            return query.ToList();
        }

        private async Task<List<ProfileFormModel>> GetFollowUpsForEmployee(string employeeId)
        {
            var fupCollection = await _fupRepository.GetAllAsync() as List<FollowUpAdHocFormEntity>;

            var query = from fup in fupCollection
                        where fup.EmployeeId == employeeId
                        select new ProfileFormModel
                        {
                            TalkType = fup.FormType,
                            FormId = fup.Id,
                            EmployeeId = fup.EmployeeId,
                            DateOfTalk = fup.DateOfTalk,
                            YearOfForm = fup.DateOfTalk.Year,
                            IsOpenForFollowUp = fup.IsOpenedForFollowUp,
                            FollowUpDate = fup.FollowUpSection.TalkDate
                        };

            return query.ToList();
        }
    }
}
