﻿using FeedbackHR.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackHR.ServiceEntities.ReportEOPService
{
    public interface IReportAfterEOPService
    {
        Task<ReportAfterEOPEntity> GetByEndOfProbationIdAsync(string EOPid);
        Task<bool> UpdateAsync(ReportAfterEOPEntity ReportEOPEntity, string EOPid);
    }
}
