﻿using Microsoft.Extensions.Options;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using FeedbackHR.DAL.Repository;

namespace FeedbackHR.DAL.Extensions
{
    public static class MongoDbExtensions
    {

        public static IMongoDatabase GetMongoDatabase(this IServiceProvider sp)
        {
            var options = sp.GetService<IOptions<DbSettings>>();
            MongoCredential credential = MongoCredential.CreateCredential
                (options.Value.MasterDatabase, options.Value.Username, options.Value.Password);
            var settings = new MongoClientSettings
            {
                Credential = credential,
                Server = new MongoServerAddress(options.Value.Server, Int32.Parse(options.Value.Port))
            };
            var client = new MongoClient(settings);
           var database = client.GetDatabase(options.Value.DatabaseName);

            return database;
        }
    }
}
