﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeedbackHR.Entities
{

    public class EndOfProbationFormEntity : IFeedbackHREntity
    {
        public string Id { get; set; }
        public FormType FormType { get; set; }
        public DateTime DateOfTalk { get; set; }
        public string HRReporter { get; set; }
        public string Client { get; set; }
        public SalaryEntity Salary { get; set; }
        public FollowUpSectionEntity FollowUpSection { get; set; } //If something is checked on front its inserted to its list or to OtherOption
        public List<FollowUpConcreteEntity> FollowUpsToDo { get; set; } // If FollowUpSection is null or if FollowUpSection.FollowUpOptions is null or empty then this list is null or empty
        public bool IsOpenedForFollowUp { get; set; } // If list of FollowUpsTo do is empty or null its false
        public string EmployeeId { get; set; }
        public string TeamLeadId { get; set; } // If Team Lead changes for Employee, to save previous Team Lead 
        public string LineManagerId { get; set; } // If Line Manager changes for Employee, to save previous Line Manager 
        public EndOfProbationConcreteEntity TeamLeadEOP { get; set; }
        public EndOfProbationConcreteEntity LineManagerEOP { get; set; }
        public EndOfProbationConcreteEntity ClientEOP { get; set; }

        public ReportAfterEOPEntity ReportEOP  { get; set; }
        public EndOfProbationFormEntity()
        {
            Id = ObjectIdFactory.NewId().ToString();
        }
    }
}
