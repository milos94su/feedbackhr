﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeedbackHR.DAL
{
    public class DbSettings
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Server { get; set; }
        public string Port { get; set; }
        public string MasterDatabase { get; set; }
    }
}
