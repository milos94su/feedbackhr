﻿using FeedbackHR.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackHR.Infrastructure.Smtp
{
    public interface IEmailSender
    {
        Task SendEOPEmail(EndOfProbationFormEntity entity);
        Task SendPATEmail(PerformanceAppraisalTalkFormEntity entity);

    }
}
