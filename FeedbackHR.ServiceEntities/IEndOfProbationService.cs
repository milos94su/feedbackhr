﻿using FeedbackHR.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackHR.ServiceEntities
{
    public interface IEndOfProbationService
    {
        Task<IEnumerable<EndOfProbationFormEntity>> GetAllAsync();
        Task<EndOfProbationFormEntity> GetByIdAsync(string EOPId);
        Task<bool> DeleteAsync(string EOPId);
        Task<bool> UpdateAsync(EndOfProbationFormEntity entity);
    }
}
