﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeedbackHR.Entities.Filter
{
    public class ProfileFormFilter
    {
        public string EmployeeId { get; set; }
        public int Year { get; set; }
        public FormType TalkType { get; set; }
        public bool? HasFollowUp { get; set; }
    }
}
