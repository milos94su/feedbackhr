﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeedbackHR.Infrastructure.Cache
{
    public class CacheSettings
    {
        public double CacheTimeApiAbsolute { get; set; }
        public double CacheTimeApiSliding { get; set; }
        public double CacheTimeDatabaseAbsolute { get; set; }
        public double CacheTimeDatabaseSliding { get; set; }
    }
}
