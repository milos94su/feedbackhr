﻿using FeedbackHR.Entities;
using FeedbackHR.Infrastructure.Cache;
using FeedbackHR.ServiceEntities;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;




namespace FeedbackHR.Infrastructure.Smtp
{
    public class EmailSender : BaseSmtp, IEmailSender
    {
        private readonly IEmailService _emailService;
 
        public EmailSender(IOptions<SmtpSettings> smtpSettings,IEmailService emailService) : base(smtpSettings) 
        {
            _emailService = emailService;
        }

        public async Task SendEOPEmail(EndOfProbationFormEntity entity)
        {
            var tlEmail = await _emailService.GetTlEmailByEmployeeId(entity.EmployeeId);
            var lmEmail = await _emailService.GetLmEmailByEmployeeId(entity.EmployeeId);
            var employeeName = await _emailService.GetNameAndLastNameById(entity.EmployeeId);

            var host = SmtpClientHost;
            int port = SmtpClientPort;
            var username = NetworkCredentialUserName;
            var password = NetworkCredentialPassword;
            SmtpClient client = new SmtpClient(host, port);
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential(username, password);
            client.EnableSsl = true;

            var mailToSend = "boris.mihajlovic@enjoying.rs";
            MailMessage mailMessage = new MailMessage();
            //mailMessage.To.Add(tlEmail);
            //mailMessage.To.Add(lmEmail);
            mailMessage.To.Add(mailToSend);
            mailMessage.From = new MailAddress(username);

            
            mailMessage.Subject = "End Of Probation";
            var body = "\n New End of probation talk is created for " + employeeName + ". Please fill report for this employee on your feedbackHR application profile.";
            mailMessage.Body = body;

            client.Send(mailMessage);
        }

        public async Task SendPATEmail(PerformanceAppraisalTalkFormEntity entity)
        {
            var tlEmail = await _emailService.GetTlEmailByEmployeeId(entity.EmployeeId);
            var lmEmail = await _emailService.GetLmEmailByEmployeeId(entity.EmployeeId);
            var employeeName = await _emailService.GetNameAndLastNameById(entity.EmployeeId);
            
            var host = SmtpClientHost;
            int port = SmtpClientPort;
            var username = NetworkCredentialUserName;
            var password = NetworkCredentialPassword;
            SmtpClient client = new SmtpClient(host, port);
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential(username, password);
            client.EnableSsl = true;

            var mailToSend = "dragana.jaksic@enjoying.rs";
            MailMessage mailMessage = new MailMessage();
            //mailMessage.To.Add(tlEmail);
            //mailMessage.To.Add(lmEmail);
            mailMessage.To.Add(mailToSend);
            mailMessage.From = new MailAddress(username);


            mailMessage.Subject = "Performance Apprasial Talk";
            var body = "\n New Permormance apprasial talk is created for "+ employeeName +". Please fill report for this employee on your feedbackhr profile.";
            mailMessage.Body = body;

            client.Send(mailMessage);
        }
    }
}
