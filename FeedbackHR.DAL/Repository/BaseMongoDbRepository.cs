﻿using FeedbackHR.DAL.Extensions;
using FeedbackHR.DAL.Extensions.Store;
using FeedbackHR.Entities;
using FeedbackHR.Repository;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FeedbackHR.DAL.Repository
{
    public class BaseMongoDbRepository<TEntity> : IRepository<TEntity>  where TEntity : IFeedbackHREntity 
    {

        static BaseMongoDbRepository()
        {
            MongoConfig.EnsureConfigured();
        }

        public BaseMongoDbRepository(IMongoDatabase database, string collectionName)
        {
            if (string.IsNullOrEmpty(collectionName))
            {
                throw new Exception();
            }

            Database = database ?? throw new ArgumentNullException(nameof(database));
            EntityCollection = database.GetCollection<TEntity>(collectionName);
        }

        protected IMongoDatabase Database { get; }
        protected IMongoCollection<TEntity> EntityCollection { get; }
        public IQueryable<TEntity> Query => EntityCollection.AsQueryable();

        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            var list = await EntityCollection.Find(FilterDefinition<TEntity>.Empty).ToListAsync();
            return list;
        }

        public async Task<TEntity> GetByIdAsync(string id)
        {
            var filter = Builders<TEntity>.Filter.Eq(x => x.Id, id);
            TEntity entity = await EntityCollection.Find(filter).FirstOrDefaultAsync();
            return entity;
        }

        public async Task<bool> InsertAsync(TEntity entity, CancellationToken cancellationToken = default)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));
            cancellationToken.ThrowIfCancellationRequested();

            await EntityCollection.InsertOneAsync(entity, cancellationToken: cancellationToken);

            var result = await GetByIdAsync(entity.Id);
            if (result != null)
                return true;
            return false;
            //return true;
        }

        public async Task<bool> DeleteByIdAsync(string id, CancellationToken cancellationToken = default)
        {
            var filter = Builders<TEntity>.Filter.Eq(x => x.Id, id);
            var result = await EntityCollection.DeleteOneAsync(filter, cancellationToken);
            return result.DeletedCount > 0;
        }

        public async Task<bool> UpdateAsync(TEntity entity, CancellationToken cancellationToken = default)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));
            cancellationToken.ThrowIfCancellationRequested();

            var filter = Builders<TEntity>.Filter.Eq(x => x.Id, entity.Id);
            var result = await EntityCollection.ReplaceOneAsync(filter, entity, cancellationToken: cancellationToken);
            return result.ModifiedCount == 1;
        }

        public async Task<PaginatedList<TEntity>> QueryPagedAsync(FilterDefinition<TEntity> filter, int currentPage = 1, int pageSize = 25, CancellationToken cancellationToken = default)
        {
            double totalDocuments = await EntityCollection.CountDocumentsAsync(filter);
            var totalPages = (int)Math.Ceiling(totalDocuments / (double)pageSize);
            var list = await EntityCollection.Find(filter).Skip((currentPage - 1) * pageSize).Limit(pageSize).ToListAsync();
            return new PaginatedList<TEntity>(list, totalPages, currentPage, pageSize);
        }

        public async Task<PaginatedList<TEntity>> QueryPagedAsync(int currentPage = 1, int pageSize = 25, CancellationToken cancellationToken = default)
        {
            return await QueryPagedAsync(FilterDefinition<TEntity>.Empty, currentPage, pageSize, cancellationToken);
        }

    }
}
