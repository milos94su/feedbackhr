﻿using FeedbackHR.DAL.Extensions.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FeedbackHR.DAL.Extensions.Store
{
    public class PaginatedList<T> : List<T>
    {
        public int CurrentPage { get; private set; }
        public int PageSize { get; private set; }
        public double TotalCount { get; private set; }
        public int TotalPages { get; private set; }

        public PaginatedList(IQueryable<T> source, int currentPage, int pageSize)
        {
            CurrentPage = currentPage;
            PageSize = pageSize;
            TotalCount = source.Count();
            TotalPages = (int)Math.Ceiling(TotalCount / (double)PageSize);

            this.AddRange(source.Skip(CurrentPage * PageSize).Take(PageSize));
        }
        public PaginatedList(IEnumerable<T> source, int totalPages, int currentPage, int pageSize)
        {
            CurrentPage = currentPage;
            PageSize = pageSize;
            TotalPages = totalPages;

            this.AddRange(source);
        }


        public PagedModel<T> AsPagedModel()
        {
            return new PagedModel<T>
            {
                Count = Count,
                CurrentPage = CurrentPage,
                TotalPages = TotalPages,
                PageSize = PageSize,
                Rows = ToArray(),
                TotalCount = TotalCount
            };
        }

        public bool HasPreviousPage
        { 
            get
            {
                return (CurrentPage > 0);
            }
        }

        public bool HasNextPage
        { 
            get
            {
                return (CurrentPage + 1 < TotalPages);
            }
        }
    }
}
