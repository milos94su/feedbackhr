﻿using FeedbackHR.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackHR.ServiceEntities
{
    public interface ICreateService<TEntity> where TEntity : IFeedbackHREntity
    {
        Task<bool> CreateAsync(TEntity entity);
        void InstanceEOP(EndOfProbationFormEntity entity);
        void InstancePAT(PerformanceAppraisalTalkFormEntity entity);
    }
}
