﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;

namespace FeedbackHR.Infrastructure.Cache
{
    public class CacheManager
    {
        private readonly MemoryCache _memoryCache;

        private const string KeySeparator = "_";
        private const string DefaultDomain = "DefaultDomain";
        public CacheSettings CacheSettings { get; set; }



        public CacheManager(IOptions<CacheSettings> cacheSettings)
        {
            _memoryCache = MemoryCache.Default;

            CacheSettings = new CacheSettings();
            CacheSettings.CacheTimeApiAbsolute = cacheSettings.Value.CacheTimeApiAbsolute;
            CacheSettings.CacheTimeApiSliding = cacheSettings.Value.CacheTimeApiSliding;
            CacheSettings.CacheTimeDatabaseAbsolute = cacheSettings.Value.CacheTimeDatabaseAbsolute;
            CacheSettings.CacheTimeDatabaseSliding = cacheSettings.Value.CacheTimeDatabaseSliding;
        }

        public void SetPermanent(object key, object value, string domain = null)
        {
            CacheItemPolicy policy = new CacheItemPolicy() { };
            Set(key, value, policy, domain);
        }

        public void SetAbsolute(object key, object value, double minutes, string domain = null)
        {
            CacheItemPolicy policy = new CacheItemPolicy()
            {
                AbsoluteExpiration = DateTime.Now + TimeSpan.FromMinutes(minutes)
            };

            Set(key, value, policy, domain);
        }

        public void SetAbsolute(object key, object value, double minutes, CacheEntryRemovedCallback callback, string domain = null)
        {
            CacheItemPolicy policy = new CacheItemPolicy()
            {
                AbsoluteExpiration = DateTime.Now + TimeSpan.FromMinutes(minutes),
                RemovedCallback = callback
            };

            Set(key, value, policy, domain);
        }

        public void SetSliding(object key, object value, double minutes, string domain = null)
        {
            CacheItemPolicy policy = new CacheItemPolicy()
            {
                SlidingExpiration = TimeSpan.FromMinutes(minutes),
            };

            Set(key, value, policy, domain);
        }
        public void Set(object key, object data, CacheItemPolicy policy, string domain = null)
        {
            _memoryCache.Set(CombineKey(key, domain), data, policy);
        }

        public T Get<T>(object key, string domain = null)
        {
            //Handle exception
            return (T)_memoryCache.Get(CombineKey(key, domain));
        }

        public object Get(object key, string domain = null)
        {
            return _memoryCache.Get(CombineKey(key, domain));
        }

        public bool Exists(object key, string domain = null)
        {
            return _memoryCache[CombineKey(key, domain)] != null;
        }

        public void Remove(object key, string domain = null)
        {
            _memoryCache.Remove(CombineKey(key, domain));
        }

        public List<object> GetAllByDomain(string domain)
        {
            var entityCollection = new List<object>();

            var result = _memoryCache.Where(e => ParseDomain(e.Key) == domain);

            foreach (KeyValuePair<string, object> item in result)
            {
                entityCollection.Add(item.Value);
            }

            return entityCollection;
        }


        #region SupportMethods
        private string CombineKey(object key, string domain)
        {
            return string.Format("{0}{1}{2}", string.IsNullOrEmpty(domain) ? DefaultDomain : domain,
                KeySeparator, key.ToString());

            //DefaultDomain_12gh4j34g4534
            //TODO: Exception handling here ?
        }

        public string ParseDomain(string combinedKey)
        {
            return combinedKey.Substring(0, combinedKey.IndexOf(KeySeparator));
        }

        public string ParseKey(string combinedKey)
        {
            return combinedKey.Substring(combinedKey.IndexOf(KeySeparator) + KeySeparator.Length);
        }


        #endregion
    }
}
