﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using DnsClient;
using FeedbackHR.Entities;
using FeedbackHR.Infrastructure.Cache;
using FeedbackHR.ServiceEntities;
using FeedbackHR.Services.DataHelpService;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Options;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using Newtonsoft.Json.Serialization;

namespace FeedbackHR.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExitInterviewController : ControllerBase
    {
        private readonly ICreateService<ExitFormEntity> _createService;
        private readonly IExitInterviewService _exitInterviewService;
        private readonly IWebHostEnvironment _webHost;
        private readonly DataCacheHelper _dataCacheHelper;

        public ExitInterviewController(ICreateService<ExitFormEntity> createService, IExitInterviewService exitInterviewService, IWebHostEnvironment webHost, IOptions<CacheSettings> options)
        {
            _dataCacheHelper = new DataCacheHelper(options);
            _createService = createService;
            _exitInterviewService = exitInterviewService;
            _webHost = webHost;
        }


        // GET: api/ExitInterview/5
        [HttpGet("{id}")]
        [Route("getExitById/{id}")]
        public async Task<ExitFormEntity> GetById(string id)
        {
            try
            {
                var exit = await _exitInterviewService.GetByIdAsync(id);

                return exit;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // POST: api/ExitInterview
        [HttpPost]
        [Route("create")]
        public async Task Create([FromForm]FormType FormType,[FromForm]DateTime DateOfTalk,[FromForm]string EmployeeId,[FromForm]string HRNotes,[FromForm]IFormFile Questionary)
        {
            
            var exitCollection = await _exitInterviewService.GetAllAsync() as List<ExitFormEntity>;

            if (exitCollection.Any(exit => exit.EmployeeId == EmployeeId))
                return;

            var exit = new ExitFormEntity()
            {
                FormType = FormType,
                DateOfTalk = DateOfTalk,
                EmployeeId = EmployeeId,
                HRNotes = HRNotes

            };

            if (Questionary != null)
            {
                string uploadPath = Path.Combine(_webHost.ContentRootPath, "uploads");
                string extension = Questionary.FileName.Split(('.'))[1];
                string fileName = EmployeeId + "_" + "Exit"+"."+extension;



                string filePath = Path.Combine(uploadPath, fileName);

                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await Questionary.CopyToAsync(stream);
                }
            }

            await _createService.CreateAsync(exit);
            
        }
        
        
        public async Task<(string fileType, byte[] archiveData, string archiveName)> GetDocxExitQuestionary(string fileName)
        {
            string filesPath = Path.Combine(_webHost.ContentRootPath, "uploads");
            var files = Directory.GetFiles(filesPath).ToList();
            var expectedFileLocation = Path.Combine(_webHost.ContentRootPath, "uploads", fileName);
            byte[] byteData;

            if (files.Any(file => file.Contains(fileName))) { 
                
                var zipName = $"{fileName}.zip";

                using (var stream = new MemoryStream())
                {
                    var file = files.FirstOrDefault(file => file.Contains(fileName));
                    var data = System.IO.File.ReadAllBytes(file);

                    stream.Write(data, 0, data.Length);
                    byteData = new byte[stream.Length];
                    byteData = stream.ToArray();

                }


                using (var ms = new MemoryStream())
                {
                    using (var archive = new ZipArchive(ms, ZipArchiveMode.Create, true))
                    {
                        var employeeCollection =await  _dataCacheHelper.GetAll<EmployeeEntity>();
                        var employeeId = fileName.Split('_')[0];
                        var employee = employeeCollection.First(employee => employee.Id == employeeId);
                        var file = files.FirstOrDefault(file => file.Contains(fileName));
                        var extension = file.Split('.')[1];

                        var newFileName = $"{employee.FirstName}_{employee.LastName}_Exit.{extension}";

                        var theFile = archive.CreateEntry(newFileName);
                        
                        using (var originalFile = new MemoryStream(byteData))
                        {
                            using(var zipEntryStream = theFile.Open())
                            {
                                originalFile.CopyTo(zipEntryStream);
                            }
                        }
                    }
                    return ("application/zip", ms.ToArray(), zipName);
                }
            }
            else
            return (null, null, null);

        }

        [HttpGet("{fileName}")]
        [Route("Stream/{fileName}")]
        public async Task<IActionResult> DownloadExit(string fileName)
        {
            try
            {
                var (fileType, archiveData, archiveName) = await GetDocxExitQuestionary(fileName);

                return File(archiveData, fileType,archiveName);
   
            }
            catch(Exception ex)
            {
                return BadRequest($"Error: {ex.Message}");
            }
        }


        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        [Route("Delete/{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            try
            {
                await _exitInterviewService.DeleteByIdAsync(id);
                return Ok();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
