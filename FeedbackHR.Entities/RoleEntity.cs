﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeedbackHR.Entities
{
    public class RoleEntity : IFeedbackHREntity
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public RoleEntity()
        {
            Id = ObjectIdFactory.NewId().ToString();
        }
    }
}
