﻿namespace FeedbackHR.Entities
{
    public enum FormType
    {   
        EndOfProbation = 1,
        PerformanceAppraisalTalk,
        AdHocMeeting,
        ExitInterview
    }
}
