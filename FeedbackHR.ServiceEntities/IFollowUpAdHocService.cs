﻿using FeedbackHR.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackHR.ServiceEntities
{
    public interface IFollowUpAdHocService
    {
        Task UpdateAsync(FollowUpAdHocFormEntity entity);
        Task<FollowUpAdHocFormEntity> GetByIdAsync(string id);
        Task<bool> DeleteByIdAsync(string id);
    }
}
