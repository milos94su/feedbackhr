﻿using FeedbackHR.Entities;
using FeedbackHR.Infrastructure.Cache;
using FeedbackHR.ServiceEntities.ServiceModels;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using FeedbackHR.Repository;
using FeedbackHR.Services.Extensions;
using System.Threading.Tasks;

namespace FeedbackHR.Services.DataHelpService
{
    public class DataMapper
    {        
        public List<EmployeeEntity> EmployeeCollection { get; set; }
        public List<UnitEntity> UnitCollection { get; set; }
        public List<TeamEntity> TeamCollection { get; set; }
        public List<LocationEntity> LocationCollection { get; set; }
        public List<EndOfProbationFormEntity> EopCollection { get; set; }
        public List<PerformanceAppraisalTalkFormEntity> PatCollection { get; set; }
        public List<FollowUpAdHocFormEntity> FollowUpCollection { get; set; }
        public List<ExitFormEntity> ExitFormCollection { get; set; }

        private DataCacheHelper dataCache;
        private IRepository<EndOfProbationFormEntity> _eopRepository;
        private IRepository<ExitFormEntity> _exitRepository;
        private IRepository<PerformanceAppraisalTalkFormEntity> _patRepository;
        private IRepository<FollowUpAdHocFormEntity> _fupRepository;
               
        public DataMapper(
            IOptions<CacheSettings> options,
            IRepository<EndOfProbationFormEntity> eopRepository,
            IRepository<ExitFormEntity> exitRepository,
            IRepository<PerformanceAppraisalTalkFormEntity> patRepository,
            IRepository<FollowUpAdHocFormEntity> fupRepository
            )
        {
            _eopRepository = eopRepository;
            _exitRepository = exitRepository;
            _patRepository = patRepository;
            _fupRepository = fupRepository;
            dataCache = new DataCacheHelper(options);
        } 
        
        public async Task<List<EmployeeFormModel>> GetAll()
        {
            var collection = new List<EmployeeFormModel>();

            collection.AddRange(await MapEmployeeEop());
            collection.AddRange(await MapEmployeeExit());
            collection.AddRange(await MapEmployeeFollowUp());
            collection.AddRange(await MapEmployeePat());

            return collection.OrderByDescending(x => x.TalkDate).ToList();
        }
        private async Task LoadData()
        {
            EmployeeCollection = await dataCache.GetAll<EmployeeEntity>();
            UnitCollection = await dataCache.GetAll<UnitEntity>();
            LocationCollection = await dataCache.GetAll<LocationEntity>();
            TeamCollection = await dataCache.GetAll<TeamEntity>();           
        }
        private async Task<List<EmployeeFormModel>> MapEmployeeEop()
        {
            await LoadData();
            EopCollection = (await _eopRepository.GetAllAsync()).ToList();
            var query = from eop in EopCollection
                        join employee in EmployeeCollection
                        on eop.EmployeeId equals employee.Id into tmpEop
                        from eopEmp in tmpEop.DefaultIfEmpty()
                        join team in TeamCollection
                        on eopEmp.TeamId equals team.Id into tmpTeam
                        from eopTeam in tmpTeam.DefaultIfEmpty()
                        join unit in UnitCollection
                        on eopEmp.UnitId equals unit.Id into tmpUnit
                        from eopUnit in tmpUnit.DefaultIfEmpty()
                        join location in LocationCollection
                        on eopEmp.LocationId equals location.Id 
                       
                        select new EmployeeFormModel {
                            EmployeeName = string.Format("{0} {1}", eopEmp.FirstName, eopEmp.LastName),
                            UnitName = (eopUnit==null) ? "Undefined": eopUnit.Name,
                            TeamName = (eopTeam == null) ? null : eopTeam.Name,
                            Location = location.Name,
                            YearsWorking = YearsWorking(eopEmp.EmploymentDate.GetDateFromString(), DateTime.Now),
                            TalkType = eop.FormType,
                            TalkDate = eop.DateOfTalk,
                            IsOpenForFollowUp = eop.IsOpenedForFollowUp,
                            FormId = eop.Id,
                            UnitId = eopEmp.UnitId,
                            TeamId = eopEmp.TeamId,
                            LocationId = eopEmp.LocationId,
                            ImageUrl = eopEmp.ImageUrl,
                            FollowUpDate = eop.FollowUpSection.TalkDate,
                            EmployeeId = eopEmp.Id
                        };

            return query.ToList();
        }

        private async Task<List<EmployeeFormModel>> MapEmployeeExit()
        {
            await LoadData();
            ExitFormCollection = (await _exitRepository.GetAllAsync()).ToList();
            var query = from exit in ExitFormCollection
                        join employee in EmployeeCollection
                        on exit.EmployeeId equals employee.Id into tmpExit
                        from exitEmp in tmpExit.DefaultIfEmpty()
                        join team in TeamCollection
                        on exitEmp.TeamId equals team.Id into tmpTeam
                        from exitTeam in tmpTeam.DefaultIfEmpty()
                        join unit in UnitCollection
                        on exitEmp.UnitId equals unit.Id into tmpUnit
                        from exitUnit in tmpUnit.DefaultIfEmpty()
                        join location in LocationCollection
                        on exitEmp.LocationId equals location.Id

                        select new EmployeeFormModel
                        {
                            EmployeeName = string.Format("{0} {1}", exitEmp.FirstName, exitEmp.LastName),
                            UnitName = (exitUnit == null) ? "Undefined" : exitUnit.Name,
                            TeamName = (exitTeam == null) ? null : exitTeam.Name,
                            Location = location.Name,
                            YearsWorking = YearsWorking(exitEmp.EmploymentDate.GetDateFromString(), DateTime.Now),
                            TalkType = exit.FormType,
                            TalkDate = exit.DateOfTalk,
                            IsOpenForFollowUp = false,
                            FormId = exit.Id,
                            UnitId = exitEmp.UnitId,
                            TeamId = exitEmp.TeamId,
                            LocationId = exitEmp.LocationId,
                            ImageUrl = exitEmp.ImageUrl
                        };

            return query.ToList();
        }

        private async Task<List<EmployeeFormModel>> MapEmployeePat()
        {
            await LoadData();
            PatCollection = (await _patRepository.GetAllAsync()).ToList();
            var query = from pat in PatCollection
                        join employee in EmployeeCollection
                        on pat.EmployeeId equals employee.Id into tmpPat
                        from patEmp in tmpPat.DefaultIfEmpty()
                        join team in TeamCollection
                        on patEmp.TeamId equals team.Id into tmpTeam
                        from patTeam in tmpTeam.DefaultIfEmpty()
                        join unit in UnitCollection
                        on patEmp.UnitId equals unit.Id into tmpUnit
                        from patUnit in tmpUnit.DefaultIfEmpty()
                        join location in LocationCollection
                        on patEmp.LocationId equals location.Id

                        select new EmployeeFormModel
                        {
                            EmployeeName = string.Format("{0} {1}", patEmp.FirstName, patEmp.LastName),
                            UnitName = (patUnit == null) ? "Undefined" : patUnit.Name,
                            TeamName = (patTeam == null) ? null : patTeam.Name,
                            Location = location.Name,
                            YearsWorking = YearsWorking(patEmp.EmploymentDate.GetDateFromString(), DateTime.Now),
                            TalkType = pat.FormType,
                            TalkDate = pat.DateOfTalk,
                            IsOpenForFollowUp = pat.IsOpenedForFollowUp,
                            FormId = pat.Id,
                            UnitId = patEmp.UnitId,
                            TeamId = patEmp.TeamId,
                            LocationId = patEmp.LocationId,
                            ImageUrl = patEmp.ImageUrl,
                            FollowUpDate = pat.FollowUpSection.TalkDate,
                            EmployeeId = patEmp.Id
                        };

            return query.ToList();
        }

        private async Task<List<EmployeeFormModel>> MapEmployeeFollowUp()
        {
            await LoadData();
            FollowUpCollection = (await _fupRepository.GetAllAsync()).ToList();
            var query = from fup in FollowUpCollection
                        join employee in EmployeeCollection
                        on fup.EmployeeId equals employee.Id into tmpFup
                        from fupEmp in tmpFup.DefaultIfEmpty()
                        join team in TeamCollection
                        on fupEmp.TeamId equals team.Id into tmpTeam
                        from fupTeam in tmpTeam.DefaultIfEmpty()
                        join unit in UnitCollection
                        on fupEmp.UnitId equals unit.Id into tmpUnit
                        from fupUnit in tmpUnit.DefaultIfEmpty()
                        join location in LocationCollection
                        on fupEmp.LocationId equals location.Id

                        select new EmployeeFormModel
                        {
                            EmployeeName = string.Format("{0} {1}", fupEmp.FirstName, fupEmp.LastName),
                            UnitName = (fupUnit == null) ? "Undefined" : fupUnit.Name,
                            TeamName = (fupTeam == null) ? null : fupTeam.Name,
                            Location = location.Name,
                            YearsWorking = YearsWorking(fupEmp.EmploymentDate.GetDateFromString(), DateTime.Now),
                            TalkType = fup.FormType,
                            TalkDate = fup.DateOfTalk,
                            IsOpenForFollowUp = fup.IsOpenedForFollowUp,
                            FormId = fup.Id,
                            UnitId = fupEmp.UnitId,
                            TeamId = fupEmp.TeamId,
                            LocationId = fupEmp.LocationId,
                            ImageUrl = fupEmp.ImageUrl,
                            FollowUpDate = fup.DateOfTalk, 
                            EmployeeId = fupEmp.Id
                        };

            return query.ToList();
        }
        private int YearsWorking(DateTime start, DateTime end)
        {
            return (end.Year - start.Year - 1) +
                (((end.Month > start.Month) ||
                ((end.Month == start.Month) && (end.Day >= start.Day))) ? 1 : 0);
        }
    }
}
