﻿using System;
using System.Collections.Generic;
using System.Text;
using FeedbackHR.Entities;
using FluentValidation;
namespace FeedbackHR.Infrastructure.Validation
{
    public class SatisfiedWithTypeEntityValidator : AbstractValidator<SatisfiedWithTypeEntity>
    {
       public SatisfiedWithTypeEntityValidator()
        {
            RuleFor(x => x.Name).NotNull().NotEmpty().WithMessage("Name must not be empty");
        }
    }
}
