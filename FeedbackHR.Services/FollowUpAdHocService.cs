﻿using FeedbackHR.Entities;
using FeedbackHR.Repository;
using FeedbackHR.ServiceEntities;
using FluentValidation;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackHR.Services
{
    public class FollowUpAdHocService : IFollowUpAdHocService
    {
        private readonly IRepository<FollowUpAdHocFormEntity> _repository;
        private readonly IValidator<FollowUpAdHocFormEntity> _validator;

        public FollowUpAdHocService(IRepository<FollowUpAdHocFormEntity> repository, IValidator<FollowUpAdHocFormEntity> validator)
        {
            _repository = repository;
            _validator = validator;
        }

        public void IsFollowUpOpened(FollowUpAdHocFormEntity entity)
        {
            entity.IsOpenedForFollowUp = false;
            for (int i = 0; i < entity.FollowUpSection.FollowUpOptions.Count; i++)
            {
                var option = entity.FollowUpSection.FollowUpOptions.ElementAt(i);
                if (option.Checked == true)
                {
                    if (entity.FollowUpsConcrete.ElementAt(i).Resolved == false)
                    {
                        entity.IsOpenedForFollowUp = true;
                    }
                }
            }
        }

        public async Task UpdateAsync(FollowUpAdHocFormEntity entity)
        {
            var valid = _validator.Validate(entity).IsValid;
            IsFollowUpOpened(entity);

            if (valid)
            {
                var result = await _repository.UpdateAsync(entity);      
            }
            else
            {
                var errors = _validator.Validate(entity).Errors;
                throw new ValidationException("Entity is not valid!", errors);
            }
        }

        public async Task<FollowUpAdHocFormEntity> GetByIdAsync(string id)
        {
            var form = await _repository.GetByIdAsync(id);

            return form;
        }
        public async Task<bool> DeleteByIdAsync(string id)
        {
            var result = await _repository.DeleteByIdAsync(id);

            return result;
        }

        
    }
}
