﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeedbackHR.Entities
{
    public class FollowUpSectionEntity : IFeedbackHREntity
    {
        public string Id { get; set; }
        public List<FollowUpTypeEntity> FollowUpOptions { get; set; }
        public string HRNotes { get; set; }
        public DateTime TalkDate { get; set; }
        public string HRReporter { get; set; }
        public FollowUpSectionEntity()
        {
            Id = ObjectIdFactory.NewId().ToString();
        }
    }
}
