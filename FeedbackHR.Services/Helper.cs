﻿using FeedbackHR.Entities;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using FeedbackHR.DAL;
using Microsoft.Extensions.Options;
using FeedbackHR.Infrastructure.Cache;

namespace FeedbackHR.Services
{
    public static class Helper<TEntity> where TEntity: IFeedbackHREntity
    {
        public static HttpClient client = new HttpClient();

        public static void Run(Api api)
        { 
            client.BaseAddress = new Uri(api.Url);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(api.Header));
        }
        private static List<TEntity> ConvertToList(JToken jToken)
        {
            var type = typeof(TEntity);
            if(type == typeof(EmployeeEntity)) return jToken["employees"].Value<JArray>().ToObject<List<TEntity>>();
            else if (type == typeof(TeamEntity)) return jToken["teams"].Value<JArray>().ToObject<List<TEntity>>();
            else if (type == typeof(UnitEntity)) return jToken["units"].Value<JArray>().ToObject<List<TEntity>>();
            else if (type == typeof(LocationEntity)) return jToken["locations"].Value<JArray>().ToObject<List<TEntity>>();
            else  return jToken["hrClassifiedPositions"].Value<JArray>().ToObject<List<TEntity>>();
           
        } 
        public static async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            List<TEntity> result = null;
            HttpResponseMessage response = await client.GetAsync(client.BaseAddress);
            if (response.IsSuccessStatusCode)
            {
                response.EnsureSuccessStatusCode();
                var responseBody = await response.Content.ReadAsStringAsync();
                JObject obj = JObject.Parse(responseBody);
                var myElement = obj.Children<JProperty>();
                var jobj = myElement.ElementAt(1).First();
                result = ConvertToList(jobj);
            }
            return result;
        }

        public static async Task<TEntity> GetByIdAsync(string id)
        {
            var list = await GetAllAsync();
            var result = list.Where(x => x.Id == id).FirstOrDefault();
            return result;
        }
    }
}