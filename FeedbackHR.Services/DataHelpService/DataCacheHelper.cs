﻿using FeedbackHR.Entities;
using FeedbackHR.Infrastructure.Cache;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackHR.Services.DataHelpService
{
   public class DataCacheHelper
    {
        private CacheManager _cacheManager;

        public DataCacheHelper(IOptions<CacheSettings> options)
        {
            _cacheManager = new CacheManager(options);
        }
        public async Task<List<T>> GetAll<T>() where T:IFeedbackHREntity
        {
            var domain = CacheHelper.GetDomainByType(typeof(T));
            var listofEntities = _cacheManager.GetAll<T>(domain);
            if (listofEntities == null || listofEntities.Count == 0)
            {
                listofEntities = (await Helper<T>.GetAllAsync()).ToList();
                foreach (var item in listofEntities)
                {
                    _cacheManager.SetEntityFromApi(item, ExpirationMode.Absolute);
                }
            }
            
            return listofEntities;
        }
    }
}
