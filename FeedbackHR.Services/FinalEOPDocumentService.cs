﻿using FeedbackHR.Entities;
using FeedbackHR.Repository;
using FeedbackHR.ServiceEntities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackHR.Services
{
    public class FinalEOPDocumentService : IFinalEOPDocumentService
    {
        private readonly IRepository<EndOfProbationFormEntity> _repository;

        public FinalEOPDocumentService(IRepository<EndOfProbationFormEntity> repository)
        {
            this._repository = repository;
        }

        public async Task<Dictionary<string, EndOfProbationConcreteEntity>> BuildFinalEOPDocument(string eopFormId)
        {
            var eopForm = await _repository.GetByIdAsync(eopFormId);

            var teamLeadEOP = eopForm.TeamLeadEOP;
            var clientEOP = eopForm.ClientEOP;
            var lineManagerEOP = eopForm.LineManagerEOP;


            var finalDocument = new Dictionary<string, EndOfProbationConcreteEntity>();
            finalDocument.Add("C", clientEOP);
            finalDocument.Add("TL", teamLeadEOP);
            finalDocument.Add("LM", lineManagerEOP);

            return finalDocument;
        }
    }
}
