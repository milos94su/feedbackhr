﻿using FeedbackHR.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedbackHR.ServiceEntities.ServiceModels
{
    public class EmployeeFormModel
    {
        public string EmployeeName { get; set; }
        public string TeamName { get; set; }
        public string UnitName { get; set; }
        public string Location { get; set; }
        public int YearsWorking { get; set; }
        public FormType TalkType { get; set; }
        public DateTime TalkDate { get; set; }
        public bool IsOpenForFollowUp { get; set; }
        public string FormId { get; set; }
        public string LocationId { get; set; }
        public string TeamId { get; set; }
        public string UnitId { get; set; }
        public string ImageUrl { get; set; }
        public DateTime FollowUpDate { get; set; }
        public string EmployeeId { get; set; }
    }
}
