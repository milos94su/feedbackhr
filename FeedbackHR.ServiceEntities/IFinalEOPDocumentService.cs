﻿using FeedbackHR.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackHR.ServiceEntities
{
    public interface IFinalEOPDocumentService
    {
        Task<Dictionary<string, EndOfProbationConcreteEntity>> BuildFinalEOPDocument(string eopFormId);
    }
}
