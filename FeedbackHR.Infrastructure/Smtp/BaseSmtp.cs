﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedbackHR.Infrastructure.Smtp
{
    public abstract class BaseSmtp 
    {
        public string SmtpClientHost { get; }
        public int SmtpClientPort { get; }
        public string NetworkCredentialUserName { get; }
        public string NetworkCredentialPassword { get; }
        public BaseSmtp(IOptions<SmtpSettings> smtpSettings)
        {
            SmtpClientHost = smtpSettings.Value.SmtpClientHost;
            SmtpClientPort = Int32.Parse(smtpSettings.Value.SmtpClientPort);
            NetworkCredentialUserName = smtpSettings.Value.NetworkCredentialUserName;
            NetworkCredentialPassword = smtpSettings.Value.NetworkCredentialPassword;
        }
    }
}
