﻿using FeedbackHR.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackHR.ServiceEntities
{
    public interface IFollowUpEOPSectionService
    {
        Task<FollowUpSectionEntity> GetByIdAsync(string EOPId);
        Task<bool> UpdateAsync(EndOfProbationFormEntity entity);
    }
}
