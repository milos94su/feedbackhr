﻿using FeedbackHR.Entities;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Bson.Serialization.Serializers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace FeedbackHR.DAL.Extensions
{
    internal static class MongoConfig
    {
        static bool _initialized = false;
        static object _initializationLock = new object();
        static object _initializationTarget;

        public static void EnsureConfigured()
        {
            EnsureConfiguredImpl();
        }

        private static void EnsureConfiguredImpl()
        {
            LazyInitializer.EnsureInitialized(ref _initializationTarget, ref _initialized, ref _initializationLock, () =>
            {
                EnsureMapperImpl();
                return null;
            });
        }

        private static bool IsConventionApplicable(Type type)
        {
            return type == typeof(EndOfProbationConcreteEntity)
                || type == typeof(EndOfProbationFormEntity)
                || type == typeof(ExitFormEntity)
                || type == typeof(FollowUpAdHocFormEntity)
                || type == typeof(FollowUpConcreteEntity)
                || type == typeof(FollowUpSectionEntity)
                || type == typeof(FollowUpTypeEntity)
                || type == typeof(PerformanceAppraisalTalkConcreteEntity)
                || type == typeof(PerformanceAppraisalTalkFormEntity)
                || type == typeof(PerformanceFactorEntity)
                || type == typeof(ReportAfterEOPEntity)
                || type == typeof(RoleEntity)
                || type == typeof(SalaryEntity)
                || type == typeof(SatisfiedWithTypeEntity)
                || type == typeof(UserEntity)
                ;
        }

        private static void RegisterConventions()
        {
            var pack = new ConventionPack()
            { 
                new IgnoreIfNullConvention(false),
                new CamelCaseElementNameConvention(),
                new EnumRepresentationConvention(BsonType.String)
            };

            ConventionRegistry.Register("FeedbackHR.DAL", pack, IsConventionApplicable);
        }

        private static void EnsureMapperImpl()
        {
            RegisterConventions();            

            BsonClassMap.RegisterClassMap<EndOfProbationConcreteEntity>(cm => {
                cm.AutoMap();
                cm.MapIdMember(c => c.Id).SetSerializer(new StringSerializer(BsonType.String)).SetIdGenerator(StringObjectIdGenerator.Instance);
            });


            BsonClassMap.RegisterClassMap<EndOfProbationFormEntity>(cm => {
                cm.AutoMap();
                cm.MapIdMember(c => c.Id).SetSerializer(new StringSerializer(BsonType.String)).SetIdGenerator(StringObjectIdGenerator.Instance);
                cm.MapMember(c => c.DateOfTalk).SetSerializer(new DateTimeSerializer(DateTimeKind.Unspecified, BsonType.Document));
            });

            BsonClassMap.RegisterClassMap<ExitFormEntity>(cm => {
                cm.AutoMap();
                cm.MapIdMember(c => c.Id).SetSerializer(new StringSerializer(BsonType.String)).SetIdGenerator(StringObjectIdGenerator.Instance);
                cm.MapMember(c => c.DateOfTalk).SetSerializer(new DateTimeSerializer(DateTimeKind.Utc, BsonType.Document));
            });

            BsonClassMap.RegisterClassMap<FollowUpAdHocFormEntity>(cm => {
                cm.AutoMap();
                cm.MapIdMember(c => c.Id).SetSerializer(new StringSerializer(BsonType.String)).SetIdGenerator(StringObjectIdGenerator.Instance);
            });

            BsonClassMap.RegisterClassMap<FollowUpConcreteEntity>(cm => {
                cm.AutoMap();
                cm.MapIdMember(c => c.Id).SetSerializer(new StringSerializer(BsonType.String)).SetIdGenerator(StringObjectIdGenerator.Instance);
            });

            BsonClassMap.RegisterClassMap<FollowUpSectionEntity>(cm => {
                cm.AutoMap();
                cm.MapIdMember(c => c.Id).SetSerializer(new StringSerializer(BsonType.String)).SetIdGenerator(StringObjectIdGenerator.Instance);
            });

            BsonClassMap.RegisterClassMap<FollowUpTypeEntity>(cm =>
            {
                cm.AutoMap();
                cm.MapIdMember(c => c.Id).SetSerializer(new StringSerializer(BsonType.String)).SetIdGenerator(StringObjectIdGenerator.Instance);
            });

            BsonClassMap.RegisterClassMap<PerformanceAppraisalTalkConcreteEntity>(cm => {
                cm.AutoMap();
                cm.MapIdMember(c => c.Id).SetSerializer(new StringSerializer(BsonType.String)).SetIdGenerator(StringObjectIdGenerator.Instance);
            });

            BsonClassMap.RegisterClassMap<PerformanceAppraisalTalkFormEntity>(cm => {
                cm.AutoMap();
                cm.MapIdMember(c => c.Id).SetSerializer(new StringSerializer(BsonType.String)).SetIdGenerator(StringObjectIdGenerator.Instance);
                cm.MapMember(c => c.DateOfTalk).SetSerializer(new DateTimeSerializer(DateTimeKind.Utc, BsonType.Document));
            });

            BsonClassMap.RegisterClassMap<PerformanceFactorEntity>(cm => {
                cm.AutoMap();
                cm.MapIdMember(c => c.Id).SetSerializer(new StringSerializer(BsonType.String)).SetIdGenerator(StringObjectIdGenerator.Instance);
            });

            BsonClassMap.RegisterClassMap<ReportAfterEOPEntity>(cm =>
            {
                cm.AutoMap();
                cm.MapIdMember(c => c.Id).SetSerializer(new StringSerializer(BsonType.String)).SetIdGenerator(StringObjectIdGenerator.Instance);
                cm.MapMember(c => c.StartDate).SetSerializer(new DateTimeSerializer(DateTimeKind.Utc, BsonType.Document));
                cm.MapMember(c => c.EndDate).SetSerializer(new DateTimeSerializer(DateTimeKind.Utc, BsonType.Document));
            });                                              

            BsonClassMap.RegisterClassMap<RoleEntity>(cm => {
                cm.AutoMap();
                cm.MapIdMember(c => c.Id).SetSerializer(new StringSerializer(BsonType.String)).SetIdGenerator(StringObjectIdGenerator.Instance);
            });

            BsonClassMap.RegisterClassMap<SalaryEntity>(cm => {
                cm.AutoMap();
                cm.MapIdMember(c => c.Id).SetSerializer(new StringSerializer(BsonType.String)).SetIdGenerator(StringObjectIdGenerator.Instance);
            });

            BsonClassMap.RegisterClassMap<SatisfiedWithTypeEntity>(cm =>
            {
                cm.AutoMap();
                cm.MapIdMember(c => c.Id).SetSerializer(new StringSerializer(BsonType.String)).SetIdGenerator(StringObjectIdGenerator.Instance);
            });

            BsonClassMap.RegisterClassMap<UserEntity>(cm => {
                cm.AutoMap();
                cm.MapIdMember(c => c.Id).SetSerializer(new StringSerializer(BsonType.String)).SetIdGenerator(StringObjectIdGenerator.Instance);
            });                        
        }        
    }
}
