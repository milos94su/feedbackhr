﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeedbackHR.Entities
{
    public class SatisfiedWithTypeEntity : IFeedbackHREntity
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public SatisfiedWithTypeEntity()
        {
            Id = ObjectIdFactory.NewId().ToString();
        }
    }
}
