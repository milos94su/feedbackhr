﻿using System;
using System.Collections.Generic;
using System.Text;
using FeedbackHR.Entities;
using FluentValidation;

namespace FeedbackHR.Infrastructure.Validation
{
   public class PerformanceAppraisalTalkFormEntityValidator : AbstractValidator<PerformanceAppraisalTalkFormEntity>
    {
        public PerformanceAppraisalTalkFormEntityValidator()
        {
            RuleFor(x => x.DateOfTalk)
              .NotEmpty().WithMessage("*Required");
            //RuleFor(x => x.Salary).NotNull().NotEmpty().WithMessage("Salary must not be empty");
            RuleFor(x => x.FormType).Equal(FormType.PerformanceAppraisalTalk).WithMessage("Form type must be PAT");
        }
    }

}
