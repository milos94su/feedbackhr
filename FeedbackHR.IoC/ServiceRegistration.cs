﻿using FeedbackHR.DAL;
using FeedbackHR.DAL.Extensions;
using FeedbackHR.DAL.Repository;
using FeedbackHR.Entities;
using FeedbackHR.Infrastructure.Cache;
using FeedbackHR.Infrastructure.Smtp;
using FeedbackHR.Repository;
using FeedbackHR.ServiceEntities;
using FeedbackHR.ServiceEntities.ReportEOPService;
using FeedbackHR.ServiceEntities.SearchService;
using FeedbackHR.ServiceEntities.SearchService.SearchEmployeesService;
using FeedbackHR.Services;
using FeedbackHR.Services.DataHelpService;
using FeedbackHR.Services.SearchService;
using FeedbackHR.Services.SearchService.SearchEmployeesService;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedbackHR.IoC
{
    public static class ServiceRegistration
    {
       public static void RegisterServices (IServiceCollection services, IConfiguration configuration)
       {

            //configuration for DbSettings
            //register services here
            services.Configure<DbSettings>(configuration.GetSection("DbSettings"));
            services.Configure<CacheSettings>(configuration.GetSection("CacheSettings"));
            services.Configure<SmtpSettings>(configuration.GetSection("SmtpSettings"));
            services.Configure<Api>(configuration.GetSection("Api"));
            services.AddTransient<IMongoDatabase>(sp => {
                return sp.GetMongoDatabase();           }
            );
            RegisterRepository(services);
            var api = configuration.GetSection("Api").Get<Api>();
            Helper<EmployeeEntity>.Run(api);
            Helper<TeamEntity>.Run(api);
            Helper<UnitEntity>.Run(api);
            Helper<LocationEntity>.Run(api);
            Helper<PositionEntity>.Run(api);

        }
        private static void RegisterRepository(IServiceCollection services)
        {
            services.AddTransient<IRepository<EndOfProbationFormEntity>>(sp => new BaseMongoDbRepository<EndOfProbationFormEntity>(sp.GetService<IMongoDatabase>(), "EndOfProbationForm"));
            services.AddTransient<IRepository<PerformanceAppraisalTalkFormEntity>>(sp => new BaseMongoDbRepository<PerformanceAppraisalTalkFormEntity>(sp.GetService<IMongoDatabase>(), "PerformanceAppraisalTalkForm"));
            services.AddTransient<IRepository<ExitFormEntity>>(sp => new BaseMongoDbRepository<ExitFormEntity>(sp.GetService<IMongoDatabase>(), "ExitForm"));
            services.AddTransient<IRepository<FollowUpAdHocFormEntity>>(sp => new BaseMongoDbRepository<FollowUpAdHocFormEntity>(sp.GetService<IMongoDatabase>(), "FollowUpAdHocForm"));
            services.AddTransient<IRepository<EmployeeEntity>>(sp => new BaseMongoDbRepository<EmployeeEntity>(sp.GetService<IMongoDatabase>(), "Employee"));
            services.AddTransient<IRepository<FollowUpTypeEntity>>(sp => new BaseMongoDbRepository<FollowUpTypeEntity>(sp.GetService<IMongoDatabase>(), "FollowUps"));
            services.AddTransient<IRepository<PositionEntity>>(sp => new BaseMongoDbRepository<PositionEntity>(sp.GetService<IMongoDatabase>(), "Position"));
            services.AddTransient<IRepository<TeamEntity>>(sp => new BaseMongoDbRepository<TeamEntity>(sp.GetService<IMongoDatabase>(), "Team"));
            services.AddTransient<IRepository<UnitEntity>>(sp => new BaseMongoDbRepository<UnitEntity>(sp.GetService<IMongoDatabase>(), "Unit"));

            services.AddTransient<IEndOfProbationService, EndOfProbationService>();
            services.AddTransient<IReportAfterEOPService, ReportAfterEOPService>();
            services.AddTransient<IEOPConcreteDocumentService, EOPConcreteDocumentService>();
            services.AddTransient<IPATConcreteDocumentService, PATConcreteDocumentService>();
            services.AddTransient<IFollowUpEOPSectionService, FollowUpEOPSectionService>();
            services.AddTransient<IFollowUpPATSectionService, FollowUpPATSectionService>();
            services.AddTransient<IPATService, PATService>();
            services.AddTransient<IFinalEOPDocumentService, FinalEOPDocumentService>();
            services.AddTransient<ISearchEmployeesService, SearchEmployees>();
            services.AddTransient<IFollowUpAdHocService, FollowUpAdHocService>();
            services.AddTransient<IExitInterviewService, ExitInterviewService>();

            services.AddTransient<IEmailService, EmailService>();
            services.AddTransient<IFinalConcreteService, FinalConcreteService>();

            services.AddTransient<ICreateService<EndOfProbationFormEntity>, CreateService<EndOfProbationFormEntity>>();
            services.AddTransient<ICreateService<PerformanceAppraisalTalkFormEntity>, CreateService<PerformanceAppraisalTalkFormEntity>>();
            services.AddTransient<ICreateService<ExitFormEntity>, CreateService<ExitFormEntity>>();
            services.AddTransient<ICreateService<FollowUpAdHocFormEntity>, CreateService<FollowUpAdHocFormEntity>>();

            services.AddTransient<ISearchServiceApi<TeamEntity>, SearchServiceApi<TeamEntity>>();
            services.AddTransient<ISearchServiceApi<UnitEntity>, SearchServiceApi<UnitEntity>>();
            services.AddTransient<ISearchServiceApi<LocationEntity>, SearchServiceApi<LocationEntity>>();
            services.AddTransient<ISearchServiceApi<PositionEntity>, SearchServiceApi<PositionEntity>>();

            services.AddTransient<IProfileDataGatherer, ProfileDataGatherer>();

            services.AddTransient<IEmailSender, EmailSender>();

        }
    }
}
