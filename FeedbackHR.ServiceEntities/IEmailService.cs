﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackHR.ServiceEntities
{
    public interface IEmailService
    {
        Task<string> GetTlEmailByEmployeeId(string id);
        Task<string> GetLmEmailByEmployeeId(string id);
        Task<string> GetNameAndLastNameById(string id);
    }
}
