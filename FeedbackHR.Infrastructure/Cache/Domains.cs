﻿namespace FeedbackHR.Infrastructure.Cache
{
    public enum Domains
    {
        Default = 0,
        Employees,
        Teams,
        Units,
        Locations,
        HrPositions,
        EndOfProbations,
        ExitInterviews,
        PerformanceAppraisalTalks,
        AdHocFollowUps,
        FollowUpOptions

    }
}
