﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeedbackHR.Infrastructure.Cache
{
    public enum ExpirationMode
    {
        Sliding = 0,

        Permanent = 1,

        Absolute = 2
    }
}
