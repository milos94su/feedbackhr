﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeedbackHR.Entities
{
    public class UnitEntity: IFeedbackHREntity
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string LineManagerId { get; set; }
        public List<TeamEntity> UnitTeams { get; set; }

       
    }
}
