﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FeedbackHR.Entities;
using FeedbackHR.ServiceEntities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FeedbackHR.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FollowUpAdHocController : ControllerBase
    {
        private readonly ICreateService<FollowUpAdHocFormEntity> _createService;
        private readonly IFollowUpAdHocService _followUpAdHocService;

        public FollowUpAdHocController(ICreateService<FollowUpAdHocFormEntity> createService, IFollowUpAdHocService followUpAdHocService)
        {
            _createService = createService;
            _followUpAdHocService = followUpAdHocService;
        }

        // GET: api/FollowUpAdHoc/5
        [HttpGet("{id}")]
        public async Task<FollowUpAdHocFormEntity> GetById(string id)
        {
            try
            {
                return await _followUpAdHocService.GetByIdAsync(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // POST: api/FollowUpAdHoc/createAdHoc
        [HttpPost]
        [Route("createAdHoc")]
        public async Task<IActionResult> Create([FromBody] FollowUpAdHocFormEntity entity)
        {
            for (int i=0; i<=14; i++)
            {
                entity.FollowUpsConcrete[i].FollowUpEntityId = entity.FollowUpSection.FollowUpOptions[i].Id;
            }

           var result= await _createService.CreateAsync(entity);
            if (result)
            {
                return Ok();
            }
            return BadRequest();
        }

        // PUT: api/FollowUpAdHoc/updateAdHoc
        [HttpPost]
        [Route("updateAdHoc")]
        public async Task<IActionResult> Update([FromBody] FollowUpAdHocFormEntity entity)
        {
            await _followUpAdHocService.UpdateAsync(entity);

            return Ok();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        [Route("Delete/{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            try
            {
                await _followUpAdHocService.DeleteByIdAsync(id);
                return NoContent();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
