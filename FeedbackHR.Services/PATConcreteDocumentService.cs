﻿using FeedbackHR.Entities;
using FeedbackHR.Infrastructure.Validation;
using FeedbackHR.Repository;
using FeedbackHR.ServiceEntities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackHR.Services
{
    public class PATConcreteDocumentService : IPATConcreteDocumentService
    {

        private IRepository<PerformanceAppraisalTalkFormEntity> _repository;
        private PerformanceAppraisalTalkConcreteEntityValidator _validator;

        public PATConcreteDocumentService(IRepository<PerformanceAppraisalTalkFormEntity> repository, PerformanceAppraisalTalkConcreteEntityValidator validator)
        {
            _repository = repository;
            _validator = validator;
        }

        private bool IsValid(PerformanceAppraisalTalkConcreteEntity entity)
        {
            var isValidate = _validator.Validate(entity).IsValid;  // Create StartDate get from FMR database API, EndDate StartDate +3 month (recomended) or insert manual
            if (!isValidate)
            {
                return false;
            }
            else return true;
        }

        public async Task<PerformanceAppraisalTalkConcreteEntity> GetByIdAsync(string ConcretePATid, string PATid)
        {
            var PAT = await _repository.GetByIdAsync(PATid);
            if (PAT != null)
            {
                if (PAT.LineManagerPAT.Id == ConcretePATid)
                {
                    return PAT.LineManagerPAT;
                }
                else if (PAT.TeamLeadPAT.Id == ConcretePATid)
                {
                    return PAT.TeamLeadPAT;
                }
                else if (PAT.ClientPAT.Id == ConcretePATid)
                {
                    return PAT.ClientPAT;
                }
            }
            return null;
        }

        public async Task<bool> UpdateAsync(PerformanceAppraisalTalkConcreteEntity concreteEntity, string PATid)
        {
            var isValid = IsValid(concreteEntity);
            var PAT = await _repository.GetByIdAsync(PATid);
            var result = false;
            if (isValid && PAT != null)
            {
                if (concreteEntity.Id == PAT.TeamLeadPAT.Id)
                {
                    PAT.TeamLeadPAT = concreteEntity;
                    result = await _repository.UpdateAsync(PAT);
                    return result;
                }
                else if (concreteEntity.Id == PAT.LineManagerPAT.Id)
                {
                    PAT.LineManagerPAT = concreteEntity;
                    result = await _repository.UpdateAsync(PAT);
                    return result;
                }
                else if (concreteEntity.Id == PAT.ClientPAT.Id)
                {
                    PAT.ClientPAT = concreteEntity;
                    result = await _repository.UpdateAsync(PAT);
                    return result;
                }
            }
            else
            {
                throw new Exception("PAT concrete is not valid od PATid does not match any from database");
            }
            return result;
        }

        public void InstancePATConcrete(PerformanceAppraisalTalkConcreteEntity entity)
        {
            entity.Quality = new PerformanceFactorEntity();
            entity.ProfessionalKnowledge = new PerformanceFactorEntity();
            entity.Reliability = new PerformanceFactorEntity();
            entity.TeamWorkAndCooperation = new PerformanceFactorEntity();
            entity.Productivity = new PerformanceFactorEntity();
            entity.Communication = new PerformanceFactorEntity();
            entity.TeamWorkAndCooperation = new PerformanceFactorEntity();
            entity.Autonomy = new PerformanceFactorEntity();
            entity.ProblemSolving = new PerformanceFactorEntity();
        }
    }
}
