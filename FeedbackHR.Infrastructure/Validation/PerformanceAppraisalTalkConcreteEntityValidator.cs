﻿using System;
using System.Collections.Generic;
using System.Text;
using FeedbackHR.Entities;
using FluentValidation;


namespace FeedbackHR.Infrastructure.Validation
{
    public class PerformanceAppraisalTalkConcreteEntityValidator : AbstractValidator<PerformanceAppraisalTalkConcreteEntity>
    {
        public PerformanceAppraisalTalkConcreteEntityValidator()
        {
            RuleFor(x => x.Quality).NotNull();
            RuleFor(x => x.Productivity).NotNull();
            RuleFor(x => x.ProfessionalKnowledge).NotNull();
            RuleFor(x => x.Reliability).NotNull();
            RuleFor(x => x.Communication).NotNull();
            RuleFor(x => x.TeamWorkAndCooperation).NotNull();
            RuleFor(x => x.Autonomy).NotNull();
            RuleFor(x => x.ProblemSolving).NotNull();
        }
    }
}
