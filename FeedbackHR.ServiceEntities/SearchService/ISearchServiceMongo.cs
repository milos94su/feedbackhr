﻿using FeedbackHR.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackHR.ServiceEntities.SearchService
{
   public interface ISearchServiceMongo<TEntity> where TEntity:IFeedbackHREntity
    {
        Task<TEntity> GetById(string Id);
        Task<List<TEntity>> GetAll();
    }
}
