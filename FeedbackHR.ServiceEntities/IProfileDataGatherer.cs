﻿using FeedbackHR.ServiceEntities.ServiceModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackHR.ServiceEntities
{
    public interface IProfileDataGatherer
    {
        Task<List<ProfileFormModel>> GetAllFormsForEmployee(string employeeId);
    }
}
