﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeedbackHR.Entities
{
    public class PerformanceAppraisalTalkFormEntity : IFeedbackHREntity
    {
        public string Id { get; set; }
        public FormType FormType { get; set; }
        public DateTime DateOfTalk { get; set; }
        public string Client { get; set; }
        public string HRReporter { get; set; }
        public SalaryEntity Salary { get; set; }
        public FollowUpSectionEntity FollowUpSection { get; set; } //If something is checked on front its inserted to its list or to OtherOption
        public List<FollowUpConcreteEntity> FollowUpsToDo { get; set; } // If FollowUpSection is null or if FollowUpSection.FollowUpOptions is null or empty then this list is null or empty
        public bool IsOpenedForFollowUp { get; set; } // If list of FollowUpsTo do is empty or null its false
        public string EmployeeId { get; set; }
        public string TeamLeadId { get; set; } // If Team Lead changes for Employee, to save previous Team Lead 
        public string LineManagerId { get; set; } // If Line Manager changes for Employee, to save previous Line Manager 

        public PerformanceAppraisalTalkConcreteEntity TeamLeadPAT { get; set; }
        public PerformanceAppraisalTalkConcreteEntity LineManagerPAT { get; set; }
        public PerformanceAppraisalTalkConcreteEntity ClientPAT { get; set; }

        public PerformanceAppraisalTalkFormEntity()
        {
            Id = ObjectIdFactory.NewId().ToString();
        }

        //TODO: Add list of EOP TL, LM, Client and FinalEOP 
    }
}
