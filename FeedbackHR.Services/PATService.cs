﻿using FeedbackHR.Entities;
using FeedbackHR.Infrastructure.Validation;
using FeedbackHR.Repository;
using FeedbackHR.ServiceEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackHR.Services
{
    public class PATService : IPATService
    {
        private IRepository<PerformanceAppraisalTalkFormEntity> _repository;
        private PerformanceAppraisalTalkFormEntityValidator _validator;

        public PATService(IRepository<PerformanceAppraisalTalkFormEntity> repository, PerformanceAppraisalTalkFormEntityValidator validator)
        {
            _repository = repository;
            _validator = validator;
        }
        private bool IsValid(PerformanceAppraisalTalkFormEntity entity)
        {
            var isValidate = _validator.Validate(entity).IsValid; 
            if (!isValidate)
            {
                return false;
            }
            else return true;
        }
        public async Task<bool> DeleteAsync(string PATId)
        {
            return await _repository.DeleteByIdAsync(PATId);
        }

        public async Task<IEnumerable<PerformanceAppraisalTalkFormEntity>> GetAllAsync()
        {
            var result = await _repository.GetAllAsync();
            return result;
        }

        public async Task<PerformanceAppraisalTalkFormEntity> GetByIdAsync(string PATId)
        {
            var result = await _repository.GetByIdAsync(PATId);
            return result;
        }

        public void IsFollowUpOpened(PerformanceAppraisalTalkFormEntity entity)
        {
            entity.IsOpenedForFollowUp = false;
            for (int i = 0; i < entity.FollowUpSection.FollowUpOptions.Count; i++)
            {
                var option = entity.FollowUpSection.FollowUpOptions.ElementAt(i);
                if (option.Checked == true)
                {
                    if (entity.FollowUpsToDo.ElementAt(i).Resolved == false)
                    {
                        entity.IsOpenedForFollowUp = true;
                    }
                }
            }
        }
        public async Task<bool> UpdateAsync(PerformanceAppraisalTalkFormEntity entity)
        {
            var isValid = IsValid(entity);
            var followUpsToDo = entity.FollowUpsToDo.Where(x=>x.Resolved==true);
            IsFollowUpOpened(entity);
            if (isValid)
            {                
                var result = await _repository.UpdateAsync(entity);
                return result;
            }
            else
            {

                throw new ArgumentException("PAT entity is not valid");
            }
        }
    }
}
