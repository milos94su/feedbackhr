﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using FeedbackHR.Entities;
using FeedbackHR.Infrastructure.Smtp;
using FeedbackHR.ServiceEntities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FeedbackHR.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PATController : ControllerBase
    {
        private readonly ICreateService<PerformanceAppraisalTalkFormEntity> _createService;
        private readonly IPATService _PATService;
        private readonly IFollowUpPATSectionService _PATSectionService;
        private readonly IPATConcreteDocumentService _PATConcreteDocument;
        private readonly IEmailSender _emailSender;
        private readonly IFinalConcreteService _finalConcrete;

        public PATController(ICreateService<PerformanceAppraisalTalkFormEntity> createService,
                             IPATService PATService, IFollowUpPATSectionService PATSectionService,
                             IPATConcreteDocumentService PATConcreteDocument,
                             IFinalConcreteService finalConcrete,
                             IEmailSender emailSender
                            )
        {
            _createService = createService;
            _PATService = PATService;
            _PATSectionService = PATSectionService;
            _PATConcreteDocument = PATConcreteDocument;
            _emailSender = emailSender;
            _finalConcrete = finalConcrete;
        }


        // GET: api/PAT
        [HttpGet]
        public async Task<IEnumerable<PerformanceAppraisalTalkFormEntity>> GetAll()
        {
            try
            {
                return await _PATService.GetAllAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // GET: api/PAT/5
        [HttpGet("{id}", Name = "Get")]
        [Route("getPatById/{id}")]
        public async Task<PerformanceAppraisalTalkFormEntity> GetById(string id)
        {
            try
            {
                return await _PATService.GetByIdAsync(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // POST: api/PAT
        [HttpPost]        
        [Route("createPAT")]
        public async Task<IActionResult> Create([FromBody]PerformanceAppraisalTalkFormEntity entity)
        {
            _createService.InstancePAT(entity);
            _PATConcreteDocument.InstancePATConcrete(entity.LineManagerPAT);
            _PATConcreteDocument.InstancePATConcrete(entity.TeamLeadPAT);
            _PATConcreteDocument.InstancePATConcrete(entity.ClientPAT);
            var result= await _createService.CreateAsync(entity);
            if (result)
            {
                await _emailSender.SendPATEmail(entity);
                return Ok();
            }
            return BadRequest();
        }

        // PUT: api/PAT/5
        [HttpPost]
        [Route("updatePAT")]
        public async Task<IActionResult> Update([FromBody]PerformanceAppraisalTalkFormEntity entity)
        {

            await _PATService.UpdateAsync(entity);
            //await _PATSectionService.UpdateAsync(id, entity.FollowUpSection);

            return Ok();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        [Route("Delete/{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            try
            {
                await _PATService.DeleteAsync(id);
                return NoContent();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet("{id}")]
        [Route("GetFinalPatMatrixByPatId/{id}")]
        public async Task<string> GetFinalPatMatrixByPatId(string id)
        {
            var entity = await _PATService.GetByIdAsync(id);
            if (entity.LineManagerPAT != null || entity.TeamLeadPAT != null || entity.ClientPAT != null)
            {
                var finalPATMatrix = await _finalConcrete.CreateFinalPATMatrix(entity.Id);
                var json = JsonSerializer.Serialize(finalPATMatrix);
                return json;
            }
                return null;
        }
        [HttpGet("{id}")]
        [Route("GetFinalPatCommentsByPatId/{id}")]
        public async Task<string> GetFinalEopCommentsByEopId(string id)
        {
            var entity = await _PATService.GetByIdAsync(id);
            var json = "";
            if (entity.LineManagerPAT != null || entity.TeamLeadPAT != null || entity.ClientPAT != null)
            {
                var finalPATComments = await _finalConcrete.CreateFinalPATComments(entity.Id);
                json = JsonSerializer.Serialize(finalPATComments);
            }
            return json;
        }
    }
}

