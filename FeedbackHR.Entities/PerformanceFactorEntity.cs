﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeedbackHR.Entities
{
    public enum PerformanceFactorMarks
    {
        NE = 1,
        U,
        IN,
        MR,
        AR
    }
    public class PerformanceFactorEntity : IFeedbackHREntity
    {
        public string Id { get; set; }
        public PerformanceFactorMarks Mark { get; set; }
        public string Remark { get; set; }

        public PerformanceFactorEntity()
        {
            Id = ObjectIdFactory.NewId().ToString();
        }
    }
}
