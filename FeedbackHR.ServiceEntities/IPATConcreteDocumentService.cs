﻿using FeedbackHR.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;


namespace FeedbackHR.ServiceEntities
{
    public interface IPATConcreteDocumentService
    {
        Task<bool> UpdateAsync(PerformanceAppraisalTalkConcreteEntity concreteEntity, string PATid);
        Task<PerformanceAppraisalTalkConcreteEntity> GetByIdAsync(string ConcretePATid, string PATid);
        void InstancePATConcrete(PerformanceAppraisalTalkConcreteEntity entity);
    }
}
