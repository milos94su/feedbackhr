﻿using FeedbackHR.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackHR.ServiceEntities
{
    public interface IExitInterviewService
    {
        Task<IEnumerable<ExitFormEntity>> GetAllAsync();
        Task<ExitFormEntity> GetByIdAsync(string id);
        Task<bool> UpdateAsync(ExitFormEntity entity);
        Task<bool> DeleteByIdAsync(string id);
    }
}
