﻿using FeedbackHR.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackHR.ServiceEntities
{
     public interface IEOPConcreteDocumentService
    {
        Task<bool> UpdateAsync(EndOfProbationConcreteEntity concreteEntity, string EOPid);
        Task <EndOfProbationConcreteEntity> GetByIdAsync(string ConcreteEOPid, string EOPid);
        void InstanceEOPConcrete(EndOfProbationConcreteEntity entity);
    }
}
