﻿using FeedbackHR.Entities;
using FeedbackHR.Infrastructure.Validation;
using FeedbackHR.Repository;
using FeedbackHR.ServiceEntities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;


namespace FeedbackHR.Services
{
     public class FollowUpPATSectionService : IFollowUpPATSectionService
    {
        private IRepository<PerformanceAppraisalTalkFormEntity> _repository;
        private FollowUpTypeEntityValidator _validator;
        public FollowUpPATSectionService(IRepository<PerformanceAppraisalTalkFormEntity> repository, FollowUpTypeEntityValidator validator)
        {
            _repository = repository;
            _validator = validator;
        }
        private bool IsValid(FollowUpTypeEntity entity)
        {
            var isValidate = _validator.Validate(entity).IsValid;
            if (!isValidate)
            {
                return false;
            }
            else return true;
        }
        public async Task<FollowUpSectionEntity> GetByIdAsync(string PATId)
        {
            var PAT = await _repository.GetByIdAsync(PATId);
            var Section = PAT.FollowUpSection;
            return Section;
        }
        public async Task<bool> UpdateAsync(string PATId, FollowUpSectionEntity entity)
        {
            var PAT = await _repository.GetByIdAsync(PATId);
            var FollowUpTypes = entity.FollowUpOptions;
            
            foreach (var FollowUp in FollowUpTypes)
            {
                var isValid = IsValid(FollowUp);
                if (isValid)
                {
                    //Creating
                    PAT.FollowUpSection.FollowUpOptions = new List<FollowUpTypeEntity>();
                    PAT.FollowUpSection.FollowUpOptions.Add(FollowUp);

                    var concrete = new FollowUpConcreteEntity();
                    concrete.FollowUpEntityId = FollowUp.Id;
                    PAT.FollowUpsToDo.Add(concrete);
                    if (FollowUp.FollowUpOption == FollowUpOptionsName.Salary)
                    {
                        concrete.SalaryAfter = new SalaryEntity();
                    }                   
                }
                else
                    throw new Exception("Follow Up is not valid");
            }
            if (PAT.FollowUpsToDo != null)
            {
                PAT.IsOpenedForFollowUp = true;
            }
            else
                PAT.IsOpenedForFollowUp = false;

            var FollowUpOptions = PAT.FollowUpSection.FollowUpOptions;
            if (FollowUpOptions != null)
            {
                return await _repository.UpdateAsync(PAT);
            }
            else
            {
                throw new ArgumentNullException("FollowUpOptions property of FollowUpSection is null");
            }
        }
    }
}
