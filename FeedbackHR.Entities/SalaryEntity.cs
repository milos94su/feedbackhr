﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeedbackHR.Entities
{
    public class SalaryEntity : IFeedbackHREntity
    {
        public string Id { get; set; }
        public bool SalaryChanged { get; set; }
        public bool IsSatisfied { get; set; }
        public int? ChangedFor { get; set; }
        public int? DesiredAmount { get; set; }

        public SalaryEntity()
        {
            Id = ObjectIdFactory.NewId().ToString();
        }
    }
}
