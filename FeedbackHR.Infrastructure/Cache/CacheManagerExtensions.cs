﻿using FeedbackHR.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedbackHR.Infrastructure.Cache
{
    public static class CacheManagerExtensions
    {
        public static void SetEntityFromDb(this CacheManager cache, IFeedbackHREntity entity, ExpirationMode expirationMode)
        {
            Domains entityDomain = CacheHelper.GetDomainByEntity(entity);

            if (expirationMode == ExpirationMode.Absolute)
                cache.SetAbsolute(entity.Id, entity, cache.CacheSettings.CacheTimeDatabaseAbsolute, entityDomain.ToString());
            if (expirationMode == ExpirationMode.Sliding)
                cache.SetSliding(entity.Id, entity, cache.CacheSettings.CacheTimeDatabaseSliding, entityDomain.ToString());
            if (expirationMode == ExpirationMode.Permanent)
                cache.SetPermanent(entity.Id, entity, entityDomain.ToString());

        }
        public static void SetEntityFromApi(this CacheManager cache, IFeedbackHREntity entity, ExpirationMode mode)
        {
            Domains entityDomain = CacheHelper.GetDomainByEntity(entity);

            if (mode == ExpirationMode.Absolute)
                cache.SetAbsolute(entity.Id, entity, cache.CacheSettings.CacheTimeApiAbsolute, entityDomain.ToString());
            if (mode == ExpirationMode.Sliding)
                cache.SetSliding(entity.Id, entity, cache.CacheSettings.CacheTimeApiSliding, entityDomain.ToString());
            if (mode == ExpirationMode.Permanent)
                cache.SetPermanent(entity.Id, entity, entityDomain.ToString());

        }

        public static List<T> GetAll<T>(this CacheManager cache, Domains domain)
        {
            var result = cache.GetAllByDomain(domain.ToString());

            List<T> entityCollection = result.ConvertAll(x => (T)x);

            return entityCollection;

        }
    }
}
