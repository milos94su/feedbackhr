﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeedbackHR.Entities
{
    public class PerformanceAppraisalTalkConcreteEntity : IFeedbackHREntity
    {
        public string Id { get; set; }

        public PerformanceFactorEntity Quality { get; set; }
        public PerformanceFactorEntity Productivity { get; set; }
        public PerformanceFactorEntity ProfessionalKnowledge { get; set; }
        public PerformanceFactorEntity Reliability { get; set; }
        public PerformanceFactorEntity Communication { get; set; }
        public PerformanceFactorEntity TeamWorkAndCooperation { get; set; }
        public PerformanceFactorEntity Autonomy { get; set; }
        public PerformanceFactorEntity ProblemSolving { get; set; }

        public string OverallImpression { get; set; }
        public string ApprasierComment { get; set; }
    
        public string PerformanceGoals { get; set; }
        public string CareerGoals { get; set; }
        public string Education { get; set; }
        public string Other { get; set; }

        public PerformanceAppraisalTalkConcreteEntity()
        {
            Id = ObjectIdFactory.NewId().ToString();
        }
    }
}
