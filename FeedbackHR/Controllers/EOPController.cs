﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FeedbackHR.Entities;
using FeedbackHR.Infrastructure.Smtp;
using FeedbackHR.ServiceEntities;
using FeedbackHR.ServiceEntities.ReportEOPService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;


namespace FeedbackHR.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EOPController : ControllerBase
    {
        private readonly ICreateService<EndOfProbationFormEntity> _createService;
        private readonly IEndOfProbationService _endOfProbationService;
        private readonly IEOPConcreteDocumentService _EOPConcreteDocument;
        private readonly IFinalConcreteService _finalConcrete;
        private readonly IEmailSender _emailSender;

        public EOPController(
            ICreateService<EndOfProbationFormEntity> createService,
            IEndOfProbationService endOfProbationService,
            IEOPConcreteDocumentService EOPConcreteDocument,
            IFinalConcreteService finalConcrete,
            IEmailSender emailSender)
        {
            _createService = createService;
            _endOfProbationService = endOfProbationService;
            _EOPConcreteDocument = EOPConcreteDocument;
            _finalConcrete = finalConcrete;
            _emailSender = emailSender;
        }

        // GET: api/EOP
        [HttpGet]
        public async Task<IEnumerable<EndOfProbationFormEntity>> GetAll()
        {
            try
            {
                return await _endOfProbationService.GetAllAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // GET: api/EOP/5
        [HttpGet("{id}")]
        [Route("GetEopById/{id}")]
        public async Task<EndOfProbationFormEntity> GetById(string id)
        {
            try
            {
                return await _endOfProbationService.GetByIdAsync(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        // POST: api/EOP
        [HttpPost]
        [Route("createEOP")]
        public async Task<IActionResult> Create([FromBody] EndOfProbationFormEntity entity)
        {
            var Eops = await GetAll();
            if(Eops.Where(x=> x.EmployeeId == entity.EmployeeId).Count()==0)
            {
                _createService.InstanceEOP(entity);
                _EOPConcreteDocument.InstanceEOPConcrete(entity.LineManagerEOP);
                _EOPConcreteDocument.InstanceEOPConcrete(entity.TeamLeadEOP);
                _EOPConcreteDocument.InstanceEOPConcrete(entity.ClientEOP);
                var result=await _createService.CreateAsync(entity);
                if (result)
                {
                    await _emailSender.SendEOPEmail(entity);
                    return Ok();
                }

            }
            return BadRequest();
        }

        // PUT: api/EOP/5
        [HttpPost]
        [Route("updateEop")]
        public async Task<IActionResult> Update([FromBody]EndOfProbationFormEntity entity)
        {
            await _endOfProbationService.UpdateAsync(entity);
            return Ok();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        [Route("Delete/{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            try
            {
                await _endOfProbationService.DeleteAsync(id);
                return Ok();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet("{id}")]
        [Route("GetFinalEopMatrixByEopId/{id}")]
        public async Task<string> GetFinalEopMatrixByEopId(string id)
        {
            var entity = await _endOfProbationService.GetByIdAsync(id);
            var json = "";
            if (entity.LineManagerEOP != null || entity.TeamLeadEOP != null || entity.ClientEOP != null)
            {
                var finalEOP = await _finalConcrete.CreateFinalEOPMatrix(entity.Id);
                json = JsonSerializer.Serialize(finalEOP);
            }
            return json;
        }
        [HttpGet("{id}")]
        [Route("GetFinalEopCommentsByEopId/{id}")]
        public async Task<string> GetFinalEopCommentsByEopId(string id)
        {
            var entity = await _endOfProbationService.GetByIdAsync(id);
            var json = "";
            if (entity.LineManagerEOP != null || entity.TeamLeadEOP != null || entity.ClientEOP != null)
            {
                var finalEOP = await _finalConcrete.CreateFinalEOPComments(entity.Id);
                json = JsonSerializer.Serialize(finalEOP);
            }
            return json;
        }
    }
}
