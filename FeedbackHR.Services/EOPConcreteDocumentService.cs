﻿using FeedbackHR.Entities;
using FeedbackHR.Infrastructure.Validation;
using FeedbackHR.Repository;
using FeedbackHR.ServiceEntities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackHR.Services
{
    public class EOPConcreteDocumentService : IEOPConcreteDocumentService
    {
        private IRepository<EndOfProbationFormEntity> _repository;
        private EndOfProbationConcreteEntityValidator _validator;

        public EOPConcreteDocumentService(IRepository<EndOfProbationFormEntity> repository, EndOfProbationConcreteEntityValidator validator)
        {
            _repository = repository;
            _validator = validator;
        }

        public void InstanceEOPConcrete(EndOfProbationConcreteEntity entity)
        {
            entity.Quality = new PerformanceFactorEntity();
            entity.ProfessionalKnowledge = new PerformanceFactorEntity();
            entity.Reliability = new PerformanceFactorEntity();
            entity.TeamWorkAndCooperation = new PerformanceFactorEntity();
            entity.Productivity = new PerformanceFactorEntity();
            entity.Communication = new PerformanceFactorEntity();
            entity.TeamWorkAndCooperation = new PerformanceFactorEntity();
            entity.Autonomy = new PerformanceFactorEntity();
        }

        private bool IsValid(EndOfProbationConcreteEntity entity)
        {
            var isValidate = _validator.Validate(entity).IsValid;  // Create StartDate get from FMR database API, EndDate StartDate +3 month (recomended) or insert manual
            if (!isValidate)
            {
                return false;
            }
            else return true;
        }

        public async Task<bool> UpdateAsync(EndOfProbationConcreteEntity concreteEntity, string EOPid)
        {
            var isValid = IsValid(concreteEntity);
            var EOP = await _repository.GetByIdAsync(EOPid);
            var result = false;
            if (isValid && EOP != null)
            {
                if (concreteEntity.Id == EOP.TeamLeadEOP.Id)
                {
                    EOP.TeamLeadEOP = concreteEntity;
                    result = await _repository.UpdateAsync(EOP);
                }
                else if (concreteEntity.Id == EOP.LineManagerEOP.Id)
                {
                    EOP.LineManagerEOP = concreteEntity;
                    result = await _repository.UpdateAsync(EOP);
                }
                else if (concreteEntity.Id == EOP.ClientEOP.Id)
                {
                    EOP.ClientEOP = concreteEntity;
                    result = await _repository.UpdateAsync(EOP);
                }
            }
            else
            {
                throw new Exception("EOP concrete is not valid or EOPid does not match any from database");
            }
            return result;
        }


        public async Task<EndOfProbationConcreteEntity> GetByIdAsync(string ConcreteEOPid, string EOPid)
        {
            var EOP = await _repository.GetByIdAsync(EOPid);
            if (EOP != null)
            {
                if (EOP.LineManagerEOP.Id == ConcreteEOPid)
                {
                    return EOP.LineManagerEOP;
                }
                else if (EOP.TeamLeadEOP.Id == ConcreteEOPid)
                {
                    return EOP.TeamLeadEOP;
                }
                else if (EOP.ClientEOP.Id == ConcreteEOPid)
                {
                    return EOP.ClientEOP;
                }
            }
            return null;
        }
    }
}
