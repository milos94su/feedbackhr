﻿using FeedbackHR.Entities;
using FeedbackHR.Repository;
using FeedbackHR.ServiceEntities.SearchService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackHR.Services.SearchService
{
    public class SearchServiceApi<TEntity> : ISearchServiceApi<TEntity> where TEntity : IFeedbackHREntity
    {
        public async Task<List<TEntity>> GetAll()
        {
            return (await Helper<TEntity>.GetAllAsync()).ToList();
        }

        public async Task<TEntity> GetById(string Id)
        {
            return await Helper<TEntity>.GetByIdAsync(Id);
        }
    }
}
