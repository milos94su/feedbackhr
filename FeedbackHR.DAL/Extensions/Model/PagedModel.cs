﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeedbackHR.DAL.Extensions.Model
{
    public class PagedModel<T>
    {
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public double TotalCount { get; set; }
        public int TotalPages { get; set; }
        public int Count { get; set; }
        public T[] Rows { get; set; }
    }
}
