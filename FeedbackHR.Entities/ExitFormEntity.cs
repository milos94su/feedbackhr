﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeedbackHR.Entities
{
    public class ExitFormEntity : IFeedbackHREntity
    {
        public string Id { get; set; }
        public FormType FormType { get; set; }
        public DateTime DateOfTalk { get; set; }
        public string CreatedById { get; set; }
        public string HRNotes { get; set; }
        public byte[] Questionary { get; set; }
        public string EmployeeId { get; set; }

        public ExitFormEntity()
        {
            Id = ObjectIdFactory.NewId().ToString();
        }

    }
}
