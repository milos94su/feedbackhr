﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeedbackHR.Entities
{
    public class FollowUpAdHocFormEntity : IFeedbackHREntity
    {
        public string Id { get; set; }
        public FormType FormType { get; set; }
        public DateTime DateOfTalk { get; set; }
        public string Reporter { get; set; }
        public bool HRIncluded { get; set; }
        public FollowUpSectionEntity FollowUpSection { get; set; }
        public List<FollowUpConcreteEntity> FollowUpsConcrete { get; set; }
        public bool IsOpenedForFollowUp { get; set; }
        public string EmployeeId { get; set; }
        public string CreatedById { get; set; }
        public SalaryEntity Salary { get; set; }

        //public SalaryEntity SalaryAfter { get; set; }
        //public SalaryEntity SalaryBefore { get; set; }
        //public List<FollowUpConcreteEntity> FollowUps { get; set; }
        //public string FollowUpOther { get; set; }


        public FollowUpAdHocFormEntity()
        {
            Id = ObjectIdFactory.NewId().ToString();
        }
    }
}
