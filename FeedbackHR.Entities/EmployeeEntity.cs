﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeedbackHR.Entities
{
    public class EmployeeEntity : IFeedbackHREntity
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string LocationId { get; set; }
        public string ImageUrl { get; set; }
        public string BirthDate { get; set; }
        public string EmploymentDate { get; set; }
        public string HrPosId { get; set; }
        public string IntPosId { get; set; }
        public string UnitId { get; set; }
        public string TeamId { get; set; }
        public bool IsLineManager { get; set; }
    }
}
