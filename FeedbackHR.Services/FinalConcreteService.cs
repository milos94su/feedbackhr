﻿using FeedbackHR.Entities;
using FeedbackHR.Repository;
using FeedbackHR.ServiceEntities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackHR.Services
{
    public class FinalConcreteService : IFinalConcreteService
    {
        private IRepository<EndOfProbationFormEntity> _repository;
        private IRepository<PerformanceAppraisalTalkFormEntity> _patRepository;  

        public FinalConcreteService(IRepository<EndOfProbationFormEntity> repository,IRepository<PerformanceAppraisalTalkFormEntity> patRepository)
        {
            _repository = repository;
            _patRepository = patRepository;
        }

        public async Task<PerformanceFactorEntity[][]> CreateFinalEOPMatrix(string EOPid)
        {
            var EOP = await _repository.GetByIdAsync(EOPid);
            var marks = new PerformanceFactorEntity[7][];
            for (int i = 0; i <= 6; i++)
            {
                marks[i] = new PerformanceFactorEntity[3];
                for(int j= 0; j <= 2; j++)
                {
                    marks[i][j] = new PerformanceFactorEntity();
                }
            }
            marks[0][0] = EOP.ClientEOP.Quality; marks[0][1] = EOP.LineManagerEOP.Quality; marks[0][2] = EOP.TeamLeadEOP.Quality;
            marks[1][0] = EOP.ClientEOP.Productivity; marks[1][1] = EOP.LineManagerEOP.Productivity; marks[1][2] = EOP.TeamLeadEOP.Productivity;
            marks[2][0] = EOP.ClientEOP.ProfessionalKnowledge; marks[2][1] = EOP.LineManagerEOP.ProfessionalKnowledge; marks[2][2] = EOP.TeamLeadEOP.ProfessionalKnowledge;
            marks[3][0] = EOP.ClientEOP.Reliability; marks[3][1] = EOP.LineManagerEOP.Reliability; marks[3][2] = EOP.TeamLeadEOP.Reliability;
            marks[4][0] = EOP.ClientEOP.Communication; marks[4][1] = EOP.LineManagerEOP.Communication; marks[4][2] = EOP.TeamLeadEOP.Communication;
            marks[5][0] = EOP.ClientEOP.TeamWorkAndCooperation; marks[5][1] = EOP.LineManagerEOP.TeamWorkAndCooperation; marks[5][2] = EOP.TeamLeadEOP.TeamWorkAndCooperation;
            marks[6][0] = EOP.ClientEOP.Autonomy; marks[6][1] = EOP.LineManagerEOP.Autonomy; marks[6][2] = EOP.TeamLeadEOP.Autonomy;

            return marks;
        }
        public async Task<string[][]> CreateFinalEOPComments(string EOPid)
        {
            var EOP = await _repository.GetByIdAsync(EOPid);
            var marks = new String[2][];
            for (int i = 0; i <= 1; i++)
            {
                marks[i] = new string[3];
                for (int j = 0; j <= 2; j++)
                {
                    marks[i][j] = "";
                }
            }
            marks[0][0] = EOP.ClientEOP.OverallImpression; marks[0][1] = EOP.LineManagerEOP.OverallImpression; marks[0][2] = EOP.TeamLeadEOP.OverallImpression;
            marks[1][0] = EOP.ClientEOP.ApprasierComment; marks[1][1] = EOP.LineManagerEOP.ApprasierComment; marks[1][2] = EOP.TeamLeadEOP.ApprasierComment;
          
            return marks;
        }
        public async Task<PerformanceFactorEntity[][]> CreateFinalPATMatrix(string PATid)
        {
            var PAT = await _patRepository.GetByIdAsync(PATid);
            var marks = new PerformanceFactorEntity[8][];
            for (int i = 0; i <= 7; i++)
            {
                marks[i] = new PerformanceFactorEntity[3];
                for (int j = 0; j <= 2; j++)
                {
                    marks[i][j] = new PerformanceFactorEntity();
                }
            }
            marks[0][0] = PAT.ClientPAT.Quality; marks[0][1] = PAT.TeamLeadPAT.Quality; marks[0][2] = PAT.LineManagerPAT.Quality;
            marks[1][0] = PAT.ClientPAT.Productivity; marks[1][1] = PAT.TeamLeadPAT.Productivity; marks[1][2] = PAT.LineManagerPAT.Productivity;
            marks[2][0] = PAT.ClientPAT.ProfessionalKnowledge; marks[2][1] = PAT.TeamLeadPAT.ProfessionalKnowledge; marks[2][2] = PAT.LineManagerPAT.ProfessionalKnowledge;
            marks[3][0] = PAT.ClientPAT.Reliability; marks[3][1] = PAT.TeamLeadPAT.Reliability; marks[3][2] = PAT.LineManagerPAT.Reliability;
            marks[4][0] = PAT.ClientPAT.Communication; marks[4][1] = PAT.TeamLeadPAT.Communication; marks[4][2] = PAT.LineManagerPAT.Communication;
            marks[5][0] = PAT.ClientPAT.TeamWorkAndCooperation; marks[5][1] = PAT.TeamLeadPAT.TeamWorkAndCooperation; marks[5][2] = PAT.LineManagerPAT.TeamWorkAndCooperation;
            marks[6][0] = PAT.ClientPAT.Autonomy; marks[6][1] = PAT.TeamLeadPAT.Autonomy; marks[6][2] = PAT.LineManagerPAT.Autonomy;
            marks[7][0] = PAT.ClientPAT.ProblemSolving; marks[7][1] = PAT.TeamLeadPAT.ProblemSolving; marks[7][2] = PAT.LineManagerPAT.ProblemSolving;
            return marks;
        }
           
        public async Task<string[][]> CreateFinalPATComments(string PATid)
        {
            var PAT = await _patRepository.GetByIdAsync(PATid);

            var marks = new String[6][];
            for (int i = 0; i <= 5; i++)
            {
                marks[i] = new string[3];
                for (int j = 0; j <= 2; j++)
                {
                    marks[i][j] = "";
                }
            }
            marks[0][0] = PAT.ClientPAT.OverallImpression; marks[0][1] = PAT.TeamLeadPAT.OverallImpression; marks[0][2] = PAT.LineManagerPAT.OverallImpression;
            marks[1][0] = PAT.ClientPAT.ApprasierComment; marks[1][1] = PAT.TeamLeadPAT.ApprasierComment; marks[1][2] = PAT.LineManagerPAT.ApprasierComment;
            marks[2][0] = PAT.ClientPAT.PerformanceGoals; marks[2][1] = PAT.TeamLeadPAT.PerformanceGoals; marks[2][2] = PAT.LineManagerPAT.PerformanceGoals;
            marks[3][0] = PAT.ClientPAT.CareerGoals; marks[3][1] = PAT.TeamLeadPAT.CareerGoals; marks[3][2] = PAT.LineManagerPAT.CareerGoals;
            marks[4][0] = PAT.ClientPAT.Education; marks[4][1] = PAT.TeamLeadPAT.Education; marks[4][2] = PAT.LineManagerPAT.Education;
            marks[5][0] = PAT.ClientPAT.Other; marks[5][1] = PAT.TeamLeadPAT.Other; marks[5][2] = PAT.LineManagerPAT.Other;
            return marks;
        }
    }
}
