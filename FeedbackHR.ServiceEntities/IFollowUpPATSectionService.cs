﻿using FeedbackHR.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackHR.ServiceEntities
{
    public interface IFollowUpPATSectionService
    {
        Task<FollowUpSectionEntity> GetByIdAsync(string PATId);
        Task<bool> UpdateAsync(string PATId, FollowUpSectionEntity entity);
    }
}
