﻿using FeedbackHR.Entities;
using FeedbackHR.Repository;
using FeedbackHR.ServiceEntities.SearchService.SearchEmployeesService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackHR.Services.SearchService.SearchEmployeesService
{
    public class SearchEmployees : ISearchEmployeesService
    {
      
        public async Task<List<EmployeeEntity>> GetEmployees()
        {
            return (await Helper<EmployeeEntity>.GetAllAsync()).ToList();
        }
        public async Task<EmployeeEntity> GetEmployeeById(string id)
        {
            return await Helper<EmployeeEntity>.GetByIdAsync(id);
        }

        public async Task<List<EmployeeEntity>> GetEmployeesFiltered(string filteredEmployee)
        {
            if (!string.IsNullOrEmpty(filteredEmployee))
            {
                filteredEmployee = filteredEmployee.ToLower();
                var employees = (await Helper<EmployeeEntity>.GetAllAsync()).ToList();
                List<EmployeeEntity> resultList;
                if (!filteredEmployee.Contains(" "))
                {
                    resultList = employees.FindAll(item => item.FirstName.ToLower().Contains(filteredEmployee) || item.LastName.ToLower().Contains(filteredEmployee));
                }
                else
                {
                        var first = filteredEmployee.Split(' ')[0];
                        var last = filteredEmployee.Split(' ')[1];
                        resultList = employees.FindAll(item => (item.FirstName.ToLower().Contains(first) && item.LastName.ToLower().Contains(last)) 
                        || (item.LastName.ToLower().Contains(first) && item.FirstName.ToLower().Contains(last)));
                    
                }
                return resultList;
            }
            else
            {
                return await GetEmployees();
            }
            
        }
    }
}
