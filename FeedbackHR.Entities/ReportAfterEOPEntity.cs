﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeedbackHR.Entities
{
    public class ReportAfterEOPEntity : IFeedbackHREntity
    {
        public string Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string EmployeesImpression { get; set; }
        public string PerformanceFeedback { get; set; }
        public string NextSteps { get; set; }
        public string FinalRecommendation { get; set; }
       

        public ReportAfterEOPEntity()
        {
            Id = ObjectIdFactory.NewId().ToString();
        }
    }
}
