﻿using FeedbackHR.Entities;
using FeedbackHR.Infrastructure.Cache;
using FeedbackHR.ServiceEntities;
using FeedbackHR.Services.DataHelpService;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackHR.Services
{
    public class EmailService : IEmailService 
    {
        private readonly DataCacheHelper _dataCache;
        public EmailService(IOptions<CacheSettings> options)
        {
            this._dataCache = new DataCacheHelper(options);
        }
        public async Task<string> GetTlEmailByEmployeeId(string id)
        {
            var employees = await _dataCache.GetAll<EmployeeEntity>();
            var employee = employees.Where(x => x.Id == id).FirstOrDefault();
            EmployeeEntity teamLead = null;
            var TlEmail = "";
            if (employee.TeamId != null)
            {
                var teams = await _dataCache.GetAll<TeamEntity>();
                var team = teams.Where(x => x.Id == employee.TeamId).FirstOrDefault();
                if (team != null && team.TeamLeadId != null)
                {
                    teamLead = employees.Where(x => x.Id == team.TeamLeadId).FirstOrDefault();
                    TlEmail = teamLead.FirstName.ToLower() + "." + teamLead.LastName.ToLower() + "@enjoying.rs";
                }
                else
                {
                    teamLead = new EmployeeEntity();
                    teamLead.FirstName = "";
                    teamLead.LastName = "";
                    TlEmail = teamLead.FirstName.ToLower() + "." + teamLead.LastName.ToLower() + "@enjoying.rs";
                }
            }
            else
            {
                teamLead = new EmployeeEntity();
                teamLead.FirstName = "";
                teamLead.LastName = "";
                TlEmail = teamLead.FirstName.ToLower() + "." + teamLead.LastName.ToLower() + "@enjoying.rs";
            }
            return TlEmail;
        }

        public async Task<string> GetLmEmailByEmployeeId(string id)
        {
            var employees = await _dataCache.GetAll<EmployeeEntity>();
            var employee = employees.Where(x => x.Id == id).FirstOrDefault();
            EmployeeEntity lineManager = null;
            var LmEmail = "";
            if (employee.UnitId != null)
            {
                var units = await _dataCache.GetAll<UnitEntity>();
                if (employee.UnitId != "0")
                {
                    lineManager = employees.Where(x => x.UnitId == employee.UnitId && x.IsLineManager == true).FirstOrDefault();
                    if(lineManager != null)
                        LmEmail = lineManager.FirstName.ToLower() + "." + lineManager.LastName.ToLower() + "@enjoying.rs";
                }
                else
                {
                    lineManager = new EmployeeEntity();
                    lineManager.FirstName = "";
                    lineManager.LastName = "";
                    LmEmail = lineManager.FirstName.ToLower() + "." + lineManager.LastName.ToLower() + "@enjoying.rs";

                }
            }
            else
            {
                lineManager = new EmployeeEntity();
                lineManager.FirstName = "";
                LmEmail = lineManager.FirstName.ToLower() + "." + lineManager.LastName.ToLower() + "@enjoying.rs";
            }
            return LmEmail;
        }
        public async Task<string> GetNameAndLastNameById(string id)
        {
            var employees = await _dataCache.GetAll<EmployeeEntity>();
            var employee = employees.Where(x => x.Id == id).FirstOrDefault();
            var result = employee.FirstName + " " + employee.LastName;
            return result;
        }
    }
}
